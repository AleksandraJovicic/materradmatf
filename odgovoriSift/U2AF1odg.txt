
Predictions

Substitution at pos 26 from A to V is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.33
    Sequences represented at this position:18

Substitution at pos 28 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.33
    Sequences represented at this position:18
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 34 from S to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.33
    Sequences represented at this position:18
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 38 from N to I is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.33
    Sequences represented at this position:18

Substitution at pos 47 from A to V is predicted to be TOLERATED with a score of 0.50.
    Median sequence conservation: 3.33
    Sequences represented at this position:18

Substitution at pos 66 from R to C is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.69
    Sequences represented at this position:12

Substitution at pos 124 from E to G is predicted to be TOLERATED with a score of 0.25.
    Median sequence conservation: 3.32
    Sequences represented at this position:18

Substitution at pos 133 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.32
    Sequences represented at this position:18
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 139 from P to L is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.32
    Sequences represented at this position:18

Substitution at pos 148 from T to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.32
    Sequences represented at this position:18
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 166 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.33
    Sequences represented at this position:17
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 167 from G to S is predicted to be TOLERATED with a score of 0.33.
    Median sequence conservation: 3.33
    Sequences represented at this position:17

Substitution at pos 170 from N to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.33
    Sequences represented at this position:17
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 172 from M to L is predicted to be TOLERATED with a score of 0.59.
    Median sequence conservation: 3.33
    Sequences represented at this position:17

Substitution at pos 184 from E to K is predicted to be TOLERATED with a score of 0.78.
    Median sequence conservation: 3.37
    Sequences represented at this position:14

Substitution at pos 189 from R to C is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.40
    Sequences represented at this position:9

Substitution at pos 194 from R to S is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.41
    Sequences represented at this position:8

Substitution at pos 203 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.43
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 209 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.45
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 225 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.43
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 234 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.43
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

