
Predictions

Substitution at pos 4 from E to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 14 from R to S is predicted to be TOLERATED with a score of 0.24.
    Median sequence conservation: 3.38
    Sequences represented at this position:20

Substitution at pos 60 from V to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.33
    Sequences represented at this position:56
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 66 from S to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.33
    Sequences represented at this position:56
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 71 from R to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.33
    Sequences represented at this position:56
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 72 from Y to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.33
    Sequences represented at this position:56
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 97 from E to K is predicted to be TOLERATED with a score of 0.62.
    Median sequence conservation: 3.33
    Sequences represented at this position:56

Substitution at pos 100 from A to T is predicted to be TOLERATED with a score of 0.24.
    Median sequence conservation: 3.33
    Sequences represented at this position:56

Substitution at pos 122 from R to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.33
    Sequences represented at this position:55
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 134 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.33
    Sequences represented at this position:56
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 154 from S to P is predicted to be TOLERATED with a score of 0.39.
    Median sequence conservation: 3.36
    Sequences represented at this position:28

Substitution at pos 163 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.35
    Sequences represented at this position:30
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 183 from G to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.34
    Sequences represented at this position:32
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 187 from I to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.38
    Sequences represented at this position:25
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 218 from D to N is predicted to be TOLERATED with a score of 0.19.
    Median sequence conservation: 3.38
    Sequences represented at this position:24

Substitution at pos 240 from V to A is predicted to be TOLERATED with a score of 0.50.
    Median sequence conservation: 3.35
    Sequences represented at this position:31

Substitution at pos 257 from K to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.39
    Sequences represented at this position:23
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 279 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.71
    Sequences represented at this position:13
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 281 from L to V is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.71
    Sequences represented at this position:13

Substitution at pos 290 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.71
    Sequences represented at this position:13
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 296 from P to S is predicted to be TOLERATED with a score of 0.76.
    Median sequence conservation: 3.71
    Sequences represented at this position:13

Substitution at pos 308 from A to E is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 3.68
    Sequences represented at this position:10

Substitution at pos 314 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.68
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 318 from H to L is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.68
    Sequences represented at this position:10

Substitution at pos 320 from P to A is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 3.72
    Sequences represented at this position:9

Substitution at pos 330 from P to Q is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.68
    Sequences represented at this position:10

Substitution at pos 338 from G to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.68
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 365 from L to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.68
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 366 from L to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.68
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 370 from Y to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.68
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

