
Predictions

Substitution at pos 14 from G to R is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.42
    Sequences represented at this position:8

Substitution at pos 84 from S to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.41
    Sequences represented at this position:7
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 109 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.41
    Sequences represented at this position:7
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 148 from D to N is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.41
    Sequences represented at this position:7

Substitution at pos 185 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.43
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 209 from L to M is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.43
    Sequences represented at this position:6

Substitution at pos 241 from V to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.43
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 246 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.43
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 254 from R to H is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.43
    Sequences represented at this position:6

Substitution at pos 256 from Q to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.43
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 271 from A to T is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.43
    Sequences represented at this position:6

Substitution at pos 274 from V to A is predicted to be TOLERATED with a score of 0.53.
    Median sequence conservation: 3.43
    Sequences represented at this position:6

Substitution at pos 323 from M to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.58
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 330 from M to I is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.58
    Sequences represented at this position:5

Substitution at pos 332 from T to M is predicted to be TOLERATED with a score of 0.14.
    Median sequence conservation: 3.58
    Sequences represented at this position:5

Substitution at pos 339 from R to Q is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.58
    Sequences represented at this position:5

Substitution at pos 347 from P to A is predicted to be TOLERATED with a score of 0.34.
    Median sequence conservation: 3.58
    Sequences represented at this position:5

Substitution at pos 359 from V to A is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.58
    Sequences represented at this position:5

Substitution at pos 406 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.38
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 425 from E to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.37
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 461 from Y to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:98

Substitution at pos 479 from D to N is predicted to be TOLERATED with a score of 0.21.
    Median sequence conservation: 2.99
    Sequences represented at this position:98

Substitution at pos 524 from P to S is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.00
    Sequences represented at this position:100

Substitution at pos 532 from S to N is predicted to be TOLERATED with a score of 0.54.
    Median sequence conservation: 2.99
    Sequences represented at this position:101

Substitution at pos 594 from E to Q is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 3.00
    Sequences represented at this position:88

Substitution at pos 635 from V to I is predicted to be TOLERATED with a score of 0.50.
    Median sequence conservation: 2.99
    Sequences represented at this position:101

Substitution at pos 649 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:101

Substitution at pos 651 from E to G is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 2.99
    Sequences represented at this position:101

Substitution at pos 682 from R to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:101

Substitution at pos 710 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.00
    Sequences represented at this position:96

Substitution at pos 733 from N to T is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 2.99
    Sequences represented at this position:101

Substitution at pos 778 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 2.99
    Sequences represented at this position:101

Substitution at pos 782 from P to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:101

Substitution at pos 813 from E to D is predicted to be TOLERATED with a score of 0.44.
    Median sequence conservation: 2.98
    Sequences represented at this position:58

