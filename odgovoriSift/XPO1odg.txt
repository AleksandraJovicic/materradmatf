
Predictions

Substitution at pos 6 from T to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 10 from D to N is predicted to be TOLERATED with a score of 0.80.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 40 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.16
    Sequences represented at this position:11

Substitution at pos 44 from R to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.16
    Sequences represented at this position:11

Substitution at pos 54 from K to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.11
    Sequences represented at this position:12

Substitution at pos 64 from D to N is predicted to be TOLERATED with a score of 0.25.
    Median sequence conservation: 3.11
    Sequences represented at this position:12

Substitution at pos 81 from Q to H is predicted to be TOLERATED with a score of 0.25.
    Median sequence conservation: 3.08
    Sequences represented at this position:14

Substitution at pos 102 from I to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.08
    Sequences represented at this position:14

Substitution at pos 166 from N to T is predicted to be TOLERATED with a score of 0.37.
    Median sequence conservation: 3.08
    Sequences represented at this position:14

Substitution at pos 171 from L to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.08
    Sequences represented at this position:14

Substitution at pos 178 from V to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.35
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 242 from F to V is predicted to be TOLERATED with a score of 0.27.
    Median sequence conservation: 3.08
    Sequences represented at this position:13

Substitution at pos 253 from K to E is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.11
    Sequences represented at this position:12

Substitution at pos 255 from L to M is predicted to be TOLERATED with a score of 0.26.
    Median sequence conservation: 3.11
    Sequences represented at this position:12

Substitution at pos 282 from Q to K is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.20
    Sequences represented at this position:10

Substitution at pos 314 from D to N is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.20
    Sequences represented at this position:10

Substitution at pos 335 from Q to K is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.20
    Sequences represented at this position:10

Substitution at pos 358 from S to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.20
    Sequences represented at this position:10

Substitution at pos 366 from F to L is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.20
    Sequences represented at this position:10

Substitution at pos 396 from G to E is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 3.46
    Sequences represented at this position:4

Substitution at pos 428 from E to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.20
    Sequences represented at this position:10

Substitution at pos 430 from V to L is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.20
    Sequences represented at this position:10

Substitution at pos 431 from L to F is predicted to be TOLERATED with a score of 0.15.
    Median sequence conservation: 3.20
    Sequences represented at this position:10

Substitution at pos 434 from E to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.20
    Sequences represented at this position:10

Substitution at pos 439 from E to D is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.20
    Sequences represented at this position:10

Substitution at pos 455 from K to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.39
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 510 from E to K is predicted to be TOLERATED with a score of 0.41.
    Median sequence conservation: 3.20
    Sequences represented at this position:10

Substitution at pos 512 from D to G is predicted to be TOLERATED with a score of 0.31.
    Median sequence conservation: 3.39
    Sequences represented at this position:5

Substitution at pos 532 from R to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.20
    Sequences represented at this position:10

Substitution at pos 535 from D to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.20
    Sequences represented at this position:10

Substitution at pos 541 from A to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.20
    Sequences represented at this position:10

Substitution at pos 547 from I to T is predicted to be TOLERATED with a score of 0.30.
    Median sequence conservation: 3.20
    Sequences represented at this position:10

Substitution at pos 563 from K to N is predicted to be TOLERATED with a score of 0.26.
    Median sequence conservation: 3.20
    Sequences represented at this position:10

Substitution at pos 572 from F to L is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 3.20
    Sequences represented at this position:10

Substitution at pos 612 from D to N is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.39
    Sequences represented at this position:5

Substitution at pos 665 from W to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.39
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 682 from P to L is predicted to be TOLERATED with a score of 0.50.
    Median sequence conservation: 3.39
    Sequences represented at this position:5

Substitution at pos 699 from C to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.39
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 721 from Y to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.39
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 732 from I to L is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.46
    Sequences represented at this position:4

Substitution at pos 735 from N to Y is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.39
    Sequences represented at this position:5

Substitution at pos 793 from P to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.39
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 800 from V to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.39
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 823 from F to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.39
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 881 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.39
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 883 from K to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.39
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 892 from T to M is predicted to be TOLERATED with a score of 0.23.
    Median sequence conservation: 3.39
    Sequences represented at this position:5

Substitution at pos 916 from Q to E is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.39
    Sequences represented at this position:5

Substitution at pos 921 from D to N is predicted to be TOLERATED with a score of 0.58.
    Median sequence conservation: 3.39
    Sequences represented at this position:5

Substitution at pos 925 from H to Q is predicted to be TOLERATED with a score of 0.40.
    Median sequence conservation: 3.39
    Sequences represented at this position:5

Substitution at pos 951 from N to T is predicted to be TOLERATED with a score of 0.54.
    Median sequence conservation: 3.39
    Sequences represented at this position:5

Substitution at pos 955 from E to A is predicted to be TOLERATED with a score of 0.15.
    Median sequence conservation: 3.39
    Sequences represented at this position:5

Substitution at pos 964 from P to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.39
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 982 from L to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.39
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1010 from A to D is predicted to be TOLERATED with a score of 0.63.
    Median sequence conservation: 3.39
    Sequences represented at this position:5

Substitution at pos 1023 from K to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.39
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1031 from S to C is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.39
    Sequences represented at this position:5

Substitution at pos 1034 from F to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.39
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1058 from G to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.39
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1062 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.39
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

