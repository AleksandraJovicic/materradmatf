
Predictions

Substitution at pos 35 from P to S is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.39
    Sequences represented at this position:9

Substitution at pos 40 from V to G is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.34
    Sequences represented at this position:9

Substitution at pos 53 from S to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.05
    Sequences represented at this position:13

Substitution at pos 61 from G to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 65 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.03
    Sequences represented at this position:22

Substitution at pos 69 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.03
    Sequences represented at this position:25

Substitution at pos 76 from T to I is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.02
    Sequences represented at this position:49

Substitution at pos 87 from G to D is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:77

Substitution at pos 96 from R to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.02
    Sequences represented at this position:77

Substitution at pos 97 from T to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.02
    Sequences represented at this position:77

Substitution at pos 105 from V to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.02
    Sequences represented at this position:77

Substitution at pos 116 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.02
    Sequences represented at this position:77

Substitution at pos 119 from Q to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.02
    Sequences represented at this position:76

Substitution at pos 132 from K to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.02
    Sequences represented at this position:77

Substitution at pos 138 from Y to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:77

Substitution at pos 146 from S to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.02
    Sequences represented at this position:76

Substitution at pos 160 from G to D is predicted to be TOLERATED with a score of 0.40.
    Median sequence conservation: 3.02
    Sequences represented at this position:77

Substitution at pos 172 from D to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:77

Substitution at pos 177 from E to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:77

Substitution at pos 181 from V to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:77

Substitution at pos 193 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:76

Substitution at pos 214 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.25
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 244 from S to P is predicted to be TOLERATED with a score of 0.26.
    Median sequence conservation: 3.39
    Sequences represented at this position:8

Substitution at pos 268 from D to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.25
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 272 from R to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.25
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 287 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.25
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 288 from G to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.07
    Sequences represented at this position:11

Substitution at pos 289 from Y to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:11

Substitution at pos 298 from N to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.07
    Sequences represented at this position:11

Substitution at pos 314 from M to I is predicted to be TOLERATED with a score of 0.18.
    Median sequence conservation: 3.07
    Sequences represented at this position:11

Substitution at pos 324 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.07
    Sequences represented at this position:11

Substitution at pos 327 from V to M is predicted to be TOLERATED with a score of 0.29.
    Median sequence conservation: 3.32
    Sequences represented at this position:10

Substitution at pos 339 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.07
    Sequences represented at this position:11

Substitution at pos 353 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:11

Substitution at pos 365 from V to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:11

Substitution at pos 376 from S to L is predicted to be TOLERATED with a score of 0.18.
    Median sequence conservation: 3.07
    Sequences represented at this position:11

Substitution at pos 377 from R to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.07
    Sequences represented at this position:11

Substitution at pos 397 from Q to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.25
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 399 from R to Q is predicted to be TOLERATED with a score of 0.16.
    Median sequence conservation: 3.25
    Sequences represented at this position:10

Substitution at pos 410 from T to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.07
    Sequences represented at this position:11

Substitution at pos 413 from S to L is predicted to be TOLERATED with a score of 0.16.
    Median sequence conservation: 3.07
    Sequences represented at this position:11

Substitution at pos 416 from G to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.07
    Sequences represented at this position:11

Substitution at pos 434 from D to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:11

Substitution at pos 437 from T to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:11

Substitution at pos 444 from S to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.07
    Sequences represented at this position:11

Substitution at pos 455 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.32
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 456 from G to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.39
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 459 from R to H is predicted to be TOLERATED with a score of 0.38.
    Median sequence conservation: 3.25
    Sequences represented at this position:10

Substitution at pos 462 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.33
    Sequences represented at this position:7
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 465 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.25
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 472 from H to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.45
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 486 from K to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.38
    Sequences represented at this position:7
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 489 from P to L is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.33
    Sequences represented at this position:7

Substitution at pos 524 from V to D is predicted to be TOLERATED with a score of 0.25.
    Median sequence conservation: 3.39
    Sequences represented at this position:9

Substitution at pos 535 from A to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.25
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 539 from V to L is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.39
    Sequences represented at this position:9

Substitution at pos 546 from G to D is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.25
    Sequences represented at this position:10

Substitution at pos 563 from E to K is predicted to be TOLERATED with a score of 0.32.
    Median sequence conservation: 3.25
    Sequences represented at this position:10

Substitution at pos 572 from G to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.38
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 577 from T to I is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.25
    Sequences represented at this position:10

Substitution at pos 611 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.25
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 620 from W to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.25
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 625 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.25
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 628 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.25
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 634 from P to L is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 3.39
    Sequences represented at this position:9

Substitution at pos 645 from P to T is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 3.25
    Sequences represented at this position:10

Substitution at pos 650 from R to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.25
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 671 from R to C is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 3.25
    Sequences represented at this position:10

Substitution at pos 693 from T to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.25
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 702 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.33
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 747 from H to N is predicted to be TOLERATED with a score of 0.14.
    Median sequence conservation: 3.32
    Sequences represented at this position:8

Substitution at pos 749 from E to D is predicted to be TOLERATED with a score of 0.71.
    Median sequence conservation: 3.32
    Sequences represented at this position:8

Substitution at pos 796 from S to N is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.07
    Sequences represented at this position:15

Substitution at pos 805 from F to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:15

Substitution at pos 809 from S to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:15

Substitution at pos 813 from A to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.07
    Sequences represented at this position:11

Substitution at pos 826 from T to M is predicted to be TOLERATED with a score of 0.15.
    Median sequence conservation: 3.07
    Sequences represented at this position:15

Substitution at pos 833 from G to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.07
    Sequences represented at this position:15

