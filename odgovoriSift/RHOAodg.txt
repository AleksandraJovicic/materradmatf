
Predictions

Substitution at pos 5 from R to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.32
    Sequences represented at this position:29
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 17 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 20 from C to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 22 from L to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 28 from D to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 34 from Y to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 37 from T to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 40 from E to D is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 41 from N to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 42 from Y to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 62 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 66 from Y to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 68 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 70 from R to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 73 from S to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 74 from Y to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 75 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 76 from D to Y is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 93 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 101 from P to S is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 108 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 137 from E to G is predicted to be TOLERATED with a score of 0.31.
    Median sequence conservation: 3.09
    Sequences represented at this position:88

Substitution at pos 161 from A to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 172 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.09
    Sequences represented at this position:89

Substitution at pos 183 from R to C is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.15
    Sequences represented at this position:63

Substitution at pos 189 from G to V is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.23
    Sequences represented at this position:57

