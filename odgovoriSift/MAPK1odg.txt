
Predictions

Substitution at pos 2 from A to V is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 46 from V to F is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

Substitution at pos 84 from I to T is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

Substitution at pos 131 from Y to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

Substitution at pos 136 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

Substitution at pos 141 from H to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

Substitution at pos 144 from N to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

Substitution at pos 148 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

Substitution at pos 162 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

Substitution at pos 167 from D to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

Substitution at pos 229 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

Substitution at pos 246 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

Substitution at pos 257 from N to S is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

Substitution at pos 266 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

Substitution at pos 271 from N to S is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

Substitution at pos 291 from D to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

Substitution at pos 313 from L to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

Substitution at pos 319 from P to S is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

Substitution at pos 320 from S to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

Substitution at pos 322 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 2.99
    Sequences represented at this position:46

