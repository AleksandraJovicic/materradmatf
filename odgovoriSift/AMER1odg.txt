
Predictions

Substitution at pos 20 from T to I is predicted to be TOLERATED with a score of 0.27.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 29 from A to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 59 from L to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 62 from G to D is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 77 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 97 from D to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 129 from S to N is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 132 from S to N is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 135 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 140 from G to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 146 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 152 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 154 from A to S is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 160 from P to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 162 from M to K is predicted to be TOLERATED with a score of 0.16.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 165 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 168 from G to S is predicted to be TOLERATED with a score of 0.78.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 174 from S to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 177 from R to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 179 from H to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 191 from E to Q is predicted to be TOLERATED with a score of 0.35.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 205 from H to Y is predicted to be TOLERATED with a score of 0.14.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 210 from S to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 223 from A to D is predicted to be TOLERATED with a score of 0.74.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 224 from P to S is predicted to be TOLERATED with a score of 0.53.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 226 from K to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 264 from A to P is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 283 from E to Q is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 318 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 348 from G to S is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 4.32
    Sequences represented at this position:6

Substitution at pos 373 from A to V is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 4.32
    Sequences represented at this position:6

Substitution at pos 377 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 4.32
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 393 from E to Q is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 4.32
    Sequences represented at this position:5

Substitution at pos 411 from E to Q is predicted to be TOLERATED with a score of 0.45.
    Median sequence conservation: 4.32
    Sequences represented at this position:6

Substitution at pos 417 from P to L is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 4.32
    Sequences represented at this position:5

Substitution at pos 418 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 453 from L to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 4.32
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 480 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 488 from G to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 491 from R to S is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 4.32
    Sequences represented at this position:6

Substitution at pos 521 from G to E is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 4.32
    Sequences represented at this position:6

Substitution at pos 542 from F to L is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 546 from L to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 548 from S to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 562 from V to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 574 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 591 from R to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 609 from Y to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 615 from T to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 616 from W to C is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 620 from G to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 622 from E to V is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 623 from A to V is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 638 from T to I is predicted to be TOLERATED with a score of 0.14.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 646 from R to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 677 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 679 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 698 from F to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 711 from K to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 713 from D to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 717 from C to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 718 from L to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 746 from P to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 756 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 826 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 834 from L to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 853 from N to K is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 857 from R to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 880 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 895 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 897 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 900 from E to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 902 from L to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 909 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 922 from Q to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 938 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 942 from R to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 954 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 988 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1002 from S to P is predicted to be TOLERATED with a score of 0.34.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 1014 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1019 from Q to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1036 from L to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1044 from M to I is predicted to be TOLERATED with a score of 0.26.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 1085 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1114 from A to D is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 1115 from E to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1119 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

