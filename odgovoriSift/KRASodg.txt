
Predictions

Substitution at pos 3 from E to K is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.17
    Sequences represented at this position:87

Substitution at pos 9 from V to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.09
    Sequences represented at this position:96

Substitution at pos 10 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:96

Substitution at pos 11 from A to V is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.09
    Sequences represented at this position:96

Substitution at pos 12 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.09
    Sequences represented at this position:97

Substitution at pos 13 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:97

Substitution at pos 17 from S to T is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 3.10
    Sequences represented at this position:98

Substitution at pos 20 from T to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.10
    Sequences represented at this position:98

Substitution at pos 21 from I to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.10
    Sequences represented at this position:98

Substitution at pos 51 from C to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.10
    Sequences represented at this position:98

Substitution at pos 54 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.10
    Sequences represented at this position:98

Substitution at pos 57 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.10
    Sequences represented at this position:98

Substitution at pos 60 from G to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.10
    Sequences represented at this position:98

Substitution at pos 61 from Q to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.10
    Sequences represented at this position:98

Substitution at pos 68 from R to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.10
    Sequences represented at this position:98

Substitution at pos 73 from R to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.10
    Sequences represented at this position:98

Substitution at pos 77 from G to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.10
    Sequences represented at this position:98

Substitution at pos 84 from I to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.10
    Sequences represented at this position:98

Substitution at pos 117 from K to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.10
    Sequences represented at this position:98

Substitution at pos 122 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.10
    Sequences represented at this position:98

Substitution at pos 123 from R to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.10
    Sequences represented at this position:98

Substitution at pos 125 from V to I is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.10
    Sequences represented at this position:98

Substitution at pos 127 from T to I is predicted to be TOLERATED with a score of 0.44.
    Median sequence conservation: 3.09
    Sequences represented at this position:97

Substitution at pos 151 from R to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.10
    Sequences represented at this position:98

Substitution at pos 158 from T to A is predicted to be TOLERATED with a score of 0.60.
    Median sequence conservation: 3.09
    Sequences represented at this position:97

Substitution at pos 162 from E to Q is predicted to be TOLERATED with a score of 0.39.
    Median sequence conservation: 3.08
    Sequences represented at this position:96

