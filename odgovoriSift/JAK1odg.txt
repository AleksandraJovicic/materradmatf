
Predictions

Substitution at pos 12 from A to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 36 from V to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.39
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 58 from E to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 83 from E to D is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 89 from Y to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 93 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 113 from F to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 128 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.16
    Sequences represented at this position:12

Substitution at pos 187 from N to S is predicted to be TOLERATED with a score of 0.36.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 193 from A to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 202 from M to T is predicted to be TOLERATED with a score of 0.15.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 260 from S to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 261 from V to L is predicted to be TOLERATED with a score of 0.44.
    Median sequence conservation: 3.13
    Sequences represented at this position:13

Substitution at pos 268 from V to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 284 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 289 from S to F is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 305 from N to D is predicted to be TOLERATED with a score of 0.64.
    Median sequence conservation: 3.34
    Sequences represented at this position:11

Substitution at pos 307 from G to S is predicted to be TOLERATED with a score of 0.86.
    Median sequence conservation: 3.34
    Sequences represented at this position:11

Substitution at pos 336 from K to N is predicted to be TOLERATED with a score of 0.49.
    Median sequence conservation: 3.36
    Sequences represented at this position:6

Substitution at pos 402 from A to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 418 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 433 from V to I is predicted to be TOLERATED with a score of 0.23.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 434 from H to Y is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 443 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 449 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 478 from T to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 483 from E to Q is predicted to be TOLERATED with a score of 0.54.
    Median sequence conservation: 3.13
    Sequences represented at this position:12

Substitution at pos 506 from R to C is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 534 from D to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.03
    Sequences represented at this position:15

Substitution at pos 542 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.07
    Sequences represented at this position:14

Substitution at pos 548 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.03
    Sequences represented at this position:15

Substitution at pos 557 from A to S is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.20
    Sequences represented at this position:12

Substitution at pos 577 from R to Q is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 606 from K to N is predicted to be TOLERATED with a score of 0.48.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 646 from S to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 659 from R to H is predicted to be TOLERATED with a score of 0.15.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 660 from D to H is predicted to be TOLERATED with a score of 0.29.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 662 from E to Q is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 696 from K to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 703 from S to I is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 723 from A to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 729 from S to C is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 737 from L to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 762 from E to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 775 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 783 from L to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 787 from C to Y is predicted to be TOLERATED with a score of 0.23.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 820 from L to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 836 from P to S is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 839 from R to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 863 from T to N is predicted to be TOLERATED with a score of 0.32.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 873 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 915 from G to E is predicted to be TOLERATED with a score of 0.22.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 996 from S to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 997 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 1003 from D to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 1033 from E to D is predicted to be TOLERATED with a score of 0.46.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 1034 from Y to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 1062 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 1097 from G to D is predicted to be TOLERATED with a score of 0.20.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 1098 from Q to H is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 1113 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 1124 from V to I is predicted to be TOLERATED with a score of 0.96.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

Substitution at pos 1133 from E to A is predicted to be TOLERATED with a score of 0.50.
    Median sequence conservation: 3.01
    Sequences represented at this position:17

