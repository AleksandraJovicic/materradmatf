
Predictions

Substitution at pos 15 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 4.09
    Sequences represented at this position:98
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 25 from T to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.05
    Sequences represented at this position:195

Substitution at pos 48 from D to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.05
    Sequences represented at this position:196

Substitution at pos 52 from K to N is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.05
    Sequences represented at this position:196

Substitution at pos 54 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.05
    Sequences represented at this position:196

Substitution at pos 69 from E to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.05
    Sequences represented at this position:196

Substitution at pos 77 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.05
    Sequences represented at this position:186

Substitution at pos 91 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.09
    Sequences represented at this position:94

Substitution at pos 92 from Y to H is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 4.22
    Sequences represented at this position:29

Substitution at pos 96 from P to L is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.12
    Sequences represented at this position:35

Substitution at pos 105 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.16
    Sequences represented at this position:9

Substitution at pos 125 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.40
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 128 from S to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.40
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 136 from S to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.40
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 162 from K to R is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.40
    Sequences represented at this position:3

Substitution at pos 167 from R to Q is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 3.40
    Sequences represented at this position:3

Substitution at pos 173 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.40
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 179 from S to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.40
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 183 from S to Y is predicted to be TOLERATED with a score of 0.28.
    Median sequence conservation: 3.40
    Sequences represented at this position:3

Substitution at pos 209 from P to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

