
Predictions

Substitution at pos 12 from M to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.33
    Sequences represented at this position:13
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 29 from Y to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 40 from P to T is predicted to be TOLERATED with a score of 0.16.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 44 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 58 from T to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 70 from H to L is predicted to be TOLERATED with a score of 0.66.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 71 from S to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 74 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.05
    Sequences represented at this position:20

Substitution at pos 83 from L to F is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 84 from E to Q is predicted to be TOLERATED with a score of 0.31.
    Median sequence conservation: 3.06
    Sequences represented at this position:25

Substitution at pos 119 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 138 from R to H is predicted to be TOLERATED with a score of 0.21.
    Median sequence conservation: 3.57
    Sequences represented at this position:10

Substitution at pos 147 from A to T is predicted to be TOLERATED with a score of 0.51.
    Median sequence conservation: 3.17
    Sequences represented at this position:22

Substitution at pos 152 from A to D is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.19
    Sequences represented at this position:23

Substitution at pos 189 from P to S is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.04
    Sequences represented at this position:25

Substitution at pos 193 from F to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:24

Substitution at pos 234 from V to G is predicted to be TOLERATED with a score of 0.38.
    Median sequence conservation: 3.33
    Sequences represented at this position:21

Substitution at pos 242 from R to H is predicted to be TOLERATED with a score of 0.19.
    Median sequence conservation: 3.08
    Sequences represented at this position:19

Substitution at pos 255 from S to A is predicted to be TOLERATED with a score of 0.52.
    Median sequence conservation: 3.04
    Sequences represented at this position:19

Substitution at pos 265 from D to N is predicted to be TOLERATED with a score of 0.29.
    Median sequence conservation: 3.13
    Sequences represented at this position:22

Substitution at pos 275 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 282 from V to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 283 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 286 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.04
    Sequences represented at this position:24

Substitution at pos 315 from S to F is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 318 from L to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 336 from S to F is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.04
    Sequences represented at this position:25

Substitution at pos 347 from Q to R is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 366 from K to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 373 from R to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 377 from S to T is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 378 from E to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 382 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 399 from S to T is predicted to be TOLERATED with a score of 0.49.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 401 from F to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 415 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 418 from A to T is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 427 from T to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 428 from E to K is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 440 from L to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 445 from E to K is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

Substitution at pos 462 from R to Q is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.04
    Sequences represented at this position:26

