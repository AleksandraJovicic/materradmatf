
Predictions

Substitution at pos 8 from A to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 21 from D to N is predicted to be TOLERATED with a score of 0.33.
    Median sequence conservation: 4.32
    Sequences represented at this position:6

Substitution at pos 31 from D to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 34 from P to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 37 from T to I is predicted to be TOLERATED with a score of 0.14.
    Median sequence conservation: 3.64
    Sequences represented at this position:9

Substitution at pos 53 from Y to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.07
    Sequences represented at this position:61

Substitution at pos 62 from V to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:61

Substitution at pos 98 from T to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.07
    Sequences represented at this position:61

Substitution at pos 109 from W to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:61

Substitution at pos 148 from T to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.07
    Sequences represented at this position:61

Substitution at pos 158 from T to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:61

Substitution at pos 191 from S to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:61

Substitution at pos 206 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:61

Substitution at pos 220 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:61

Substitution at pos 235 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:61

Substitution at pos 265 from D to N is predicted to be TOLERATED with a score of 0.57.
    Median sequence conservation: 3.07
    Sequences represented at this position:61

Substitution at pos 278 from H to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:61

Substitution at pos 291 from Q to H is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.11
    Sequences represented at this position:58

Substitution at pos 302 from V to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:61

Substitution at pos 343 from F to L is predicted to be TOLERATED with a score of 0.64.
    Median sequence conservation: 3.07
    Sequences represented at this position:61

Substitution at pos 374 from G to V is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 3.38
    Sequences represented at this position:12

Substitution at pos 393 from G to R is predicted to be TOLERATED with a score of 0.16.
    Median sequence conservation: 4.32
    Sequences represented at this position:5

