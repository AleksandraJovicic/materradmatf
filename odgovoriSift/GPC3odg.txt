
Predictions

Substitution at pos 2 from A to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 5 from V to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 29 from P to L is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 3.50
    Sequences represented at this position:4

Substitution at pos 44 from R to S is predicted to be TOLERATED with a score of 0.58.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 50 from K to R is predicted to be TOLERATED with a score of 0.50.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 68 from K to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 73 from C to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 93 from L to I is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 95 from S to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 112 from Q to P is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 119 from V to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 120 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 121 from H to R is predicted to be TOLERATED with a score of 0.54.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 161 from I to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 164 from D to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 166 from M to I is predicted to be TOLERATED with a score of 0.40.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 198 from L to F is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 202 from R to I is predicted to be TOLERATED with a score of 0.21.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 206 from K to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 216 from M to V is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 294 from K to E is predicted to be TOLERATED with a score of 0.66.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 313 from I to M is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 317 from E to K is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 326 from T to K is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 357 from I to T is predicted to be TOLERATED with a score of 0.16.
    Median sequence conservation: 3.39
    Sequences represented at this position:7

Substitution at pos 369 from G to V is predicted to be TOLERATED with a score of 0.54.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 387 from E to Q is predicted to be TOLERATED with a score of 0.35.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 397 from K to T is predicted to be TOLERATED with a score of 0.88.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 420 from S to Y is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 437 from P to R is predicted to be TOLERATED with a score of 0.68.
    Median sequence conservation: 3.01
    Sequences represented at this position:13

Substitution at pos 498 from T to A is predicted to be TOLERATED with a score of 0.58.
    Median sequence conservation: 3.20
    Sequences represented at this position:6

Substitution at pos 508 from D to V is predicted to be TOLERATED with a score of 0.40.
    Median sequence conservation: 3.33
    Sequences represented at this position:5

Substitution at pos 567 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 584 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 587 from L to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

