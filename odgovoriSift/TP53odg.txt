
Predictions

Substitution at pos 8 from P to T is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.73
    Sequences represented at this position:17

Substitution at pos 15 from S to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.40
    Sequences represented at this position:20
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 23 from W to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.33
    Sequences represented at this position:23
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 41 from D to N is predicted to be TOLERATED with a score of 0.44.
    Median sequence conservation: 3.33
    Sequences represented at this position:24

Substitution at pos 46 from S to F is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.53
    Sequences represented at this position:17

Substitution at pos 48 from D to N is predicted to be TOLERATED with a score of 0.21.
    Median sequence conservation: 3.41
    Sequences represented at this position:20

Substitution at pos 49 from D to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.41
    Sequences represented at this position:19
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 57 from D to N is predicted to be TOLERATED with a score of 0.27.
    Median sequence conservation: 3.42
    Sequences represented at this position:20

Substitution at pos 73 from V to L is predicted to be TOLERATED with a score of 0.48.
    Median sequence conservation: 3.27
    Sequences represented at this position:23

Substitution at pos 85 from P to T is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.29
    Sequences represented at this position:27

Substitution at pos 97 from V to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.22
    Sequences represented at this position:32

Substitution at pos 105 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:34

Substitution at pos 106 from S to R is predicted to be TOLERATED with a score of 0.50.
    Median sequence conservation: 3.14
    Sequences represented at this position:34

Substitution at pos 107 from Y to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.14
    Sequences represented at this position:34

Substitution at pos 109 from F to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.14
    Sequences represented at this position:34

Substitution at pos 111 from L to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:34

Substitution at pos 113 from F to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:34

Substitution at pos 114 from L to F is predicted to be TOLERATED with a score of 0.20.
    Median sequence conservation: 3.14
    Sequences represented at this position:34

Substitution at pos 124 from C to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.14
    Sequences represented at this position:34

Substitution at pos 125 from T to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:34

Substitution at pos 127 from S to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 128 from P to A is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 130 from L to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 133 from M to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 137 from L to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 141 from C to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 142 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 143 from V to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 147 from V to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 151 from P to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 152 from P to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 154 from G to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 157 from V to L is predicted to be TOLERATED with a score of 0.51.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 159 from A to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 160 from M to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 161 from A to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 162 from I to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 163 from Y to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 166 from S to I is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 168 from H to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 169 from M to I is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 173 from V to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 175 from R to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 176 from C to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 181 from R to S is predicted to be TOLERATED with a score of 0.18.
    Median sequence conservation: 3.22
    Sequences represented at this position:32

Substitution at pos 182 from C to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.22
    Sequences represented at this position:32

Substitution at pos 187 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 189 from A to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.15
    Sequences represented at this position:33

Substitution at pos 190 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.15
    Sequences represented at this position:33

Substitution at pos 191 from P to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 192 from Q to K is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 193 from H to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 194 from L to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 196 from R to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 203 from V to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 205 from Y to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 208 from D to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 213 from R to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 215 from S to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 216 from V to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 220 from Y to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 223 from P to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 226 from G to D is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 232 from I to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 234 from Y to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 236 from Y to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 238 from C to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 239 from N to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 241 from S to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 242 from C to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 244 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 245 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 246 from M to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 248 from R to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 253 from T to I is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 254 from I to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 255 from I to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 256 from T to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 257 from L to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 258 from E to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 261 from S to R is predicted to be TOLERATED with a score of 0.42.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 262 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 265 from L to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 266 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 267 from R to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 270 from F to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 272 from V to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 273 from R to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 274 from V to I is predicted to be TOLERATED with a score of 0.35.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 275 from C to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 276 from A to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 278 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 279 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 280 from R to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 281 from D to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 282 from R to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 283 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 284 from T to A is predicted to be TOLERATED with a score of 0.21.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 286 from E to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 289 from L to F is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 290 from R to H is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 293 from G to V is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.27
    Sequences represented at this position:32

Substitution at pos 295 from P to T is predicted to be TOLERATED with a score of 0.89.
    Median sequence conservation: 3.22
    Sequences represented at this position:33

Substitution at pos 300 from P to S is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 3.20
    Sequences represented at this position:33

Substitution at pos 304 from T to A is predicted to be TOLERATED with a score of 0.44.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 306 from R to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 307 from A to V is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 314 from S to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 317 from Q to R is predicted to be TOLERATED with a score of 0.53.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 319 from K to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.14
    Sequences represented at this position:35

Substitution at pos 320 from K to E is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.15
    Sequences represented at this position:33

Substitution at pos 330 from L to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:34

Substitution at pos 331 from Q to R is predicted to be TOLERATED with a score of 0.27.
    Median sequence conservation: 3.14
    Sequences represented at this position:34

Substitution at pos 333 from R to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.14
    Sequences represented at this position:33

Substitution at pos 337 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.14
    Sequences represented at this position:32

Substitution at pos 341 from F to V is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.14
    Sequences represented at this position:32

Substitution at pos 342 from R to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.14
    Sequences represented at this position:32

Substitution at pos 352 from D to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.25
    Sequences represented at this position:27

Substitution at pos 358 from E to Q is predicted to be TOLERATED with a score of 0.14.
    Median sequence conservation: 3.32
    Sequences represented at this position:22

Substitution at pos 371 from S to P is predicted to be TOLERATED with a score of 0.23.
    Median sequence conservation: 3.40
    Sequences represented at this position:21

Substitution at pos 377 from T to S is predicted to be TOLERATED with a score of 0.82.
    Median sequence conservation: 3.55
    Sequences represented at this position:19

Substitution at pos 381 from K to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.45
    Sequences represented at this position:20
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 385 from F to L is predicted to be TOLERATED with a score of 0.54.
    Median sequence conservation: 3.45
    Sequences represented at this position:20

Substitution at pos 390 from P to L is predicted to be TOLERATED with a score of 0.37.
    Median sequence conservation: 3.45
    Sequences represented at this position:20

