
Predictions

Substitution at pos 2 from Y to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 6 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 11 from S to L is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 15 from V to L is predicted to be TOLERATED with a score of 0.75.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 18 from P to S is predicted to be TOLERATED with a score of 0.47.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 46 from S to R is predicted to be TOLERATED with a score of 0.14.
    Median sequence conservation: 3.40
    Sequences represented at this position:13

Substitution at pos 51 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.03
    Sequences represented at this position:91

Substitution at pos 70 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 73 from V to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 82 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 98 from I to M is predicted to be TOLERATED with a score of 0.25.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 100 from K to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 102 from A to V is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 103 from A to T is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 110 from Y to H is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 114 from P to S is predicted to be TOLERATED with a score of 0.33.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 119 from A to T is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 144 from G to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 167 from G to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:81

Substitution at pos 186 from S to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 218 from L to F is predicted to be TOLERATED with a score of 0.25.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 235 from H to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 237 from Q to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 246 from Q to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 268 from R to I is predicted to be TOLERATED with a score of 0.25.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 273 from A to P is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 285 from T to S is predicted to be TOLERATED with a score of 0.18.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 293 from V to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 294 from A to V is predicted to be TOLERATED with a score of 0.16.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 300 from L to H is predicted to be TOLERATED with a score of 0.54.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 309 from P to S is predicted to be TOLERATED with a score of 0.26.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 313 from E to D is predicted to be TOLERATED with a score of 0.18.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 320 from A to T is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 334 from S to G is predicted to be TOLERATED with a score of 0.15.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 340 from N to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 351 from S to T is predicted to be TOLERATED with a score of 0.35.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 367 from I to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 376 from Q to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 378 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 380 from M to V is predicted to be TOLERATED with a score of 0.36.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 386 from Q to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 445 from I to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 463 from I to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 474 from T to R is predicted to be TOLERATED with a score of 0.36.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 486 from A to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

Substitution at pos 487 from I to V is predicted to be TOLERATED with a score of 0.55.
    Median sequence conservation: 2.99
    Sequences represented at this position:92

