
Predictions

Substitution at pos 35 from D to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 36 from P to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 52 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 70 from G to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 71 from S to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 98 from G to S is predicted to be TOLERATED with a score of 0.66.
    Median sequence conservation: 4.32
    Sequences represented at this position:4

Substitution at pos 103 from A to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 132 from P to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 135 from P to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 138 from S to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 139 from F to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 143 from E to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 148 from G to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 149 from A to S is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 4.32
    Sequences represented at this position:4

Substitution at pos 157 from L to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 160 from F to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 176 from R to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 186 from S to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 188 from A to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 194 from R to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 200 from P to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 239 from F to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 242 from H to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 249 from P to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 266 from P to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 297 from M to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 312 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 317 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 334 from N to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 4.32
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 342 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 352 from A to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:15
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 354 from Y to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:15
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 360 from G to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:33
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 361 from V to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 4.32
    Sequences represented at this position:33
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 393 from C to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:345
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 400 from K to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:369
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 426 from K to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 4.32
    Sequences represented at this position:209
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 430 from R to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 4.32
    Sequences represented at this position:395
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 431 from R to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 4.32
    Sequences represented at this position:395
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 434 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:395
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 438 from L to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:395
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 443 from R to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 4.32
    Sequences represented at this position:395
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 447 from G to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 4.32
    Sequences represented at this position:395
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 450 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:395
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 458 from R to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:395
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 461 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:395
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 462 from R to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 4.32
    Sequences represented at this position:395
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 465 from H to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 4.32
    Sequences represented at this position:395
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 474 from T to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:315
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 492 from K to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 4.32
    Sequences represented at this position:314
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 501 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:304
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

