
Predictions

Substitution at pos 2 from A to T is predicted to be TOLERATED with a score of 0.16.
    Median sequence conservation: 3.33
    Sequences represented at this position:12

Substitution at pos 15 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.00
    Sequences represented at this position:78

Substitution at pos 43 from G to E is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.00
    Sequences represented at this position:75

Substitution at pos 99 from D to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.00
    Sequences represented at this position:79

Substitution at pos 127 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.00
    Sequences represented at this position:79

Substitution at pos 142 from K to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.00
    Sequences represented at this position:79

Substitution at pos 161 from L to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.00
    Sequences represented at this position:79

Substitution at pos 196 from D to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.00
    Sequences represented at this position:79

Substitution at pos 206 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.00
    Sequences represented at this position:79

Substitution at pos 246 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.00
    Sequences represented at this position:78

Substitution at pos 255 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.00
    Sequences represented at this position:79

Substitution at pos 288 from R to Q is predicted to be TOLERATED with a score of 0.89.
    Median sequence conservation: 3.00
    Sequences represented at this position:79

Substitution at pos 296 from H to Y is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.22
    Sequences represented at this position:25

