
Predictions

Substitution at pos 37 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 59 from F to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.48
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 62 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.48
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 85 from S to N is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.48
    Sequences represented at this position:8

Substitution at pos 87 from K to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.48
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 89 from S to A is predicted to be TOLERATED with a score of 0.23.
    Median sequence conservation: 3.48
    Sequences represented at this position:8

Substitution at pos 91 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.48
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 107 from P to L is predicted to be TOLERATED with a score of 0.28.
    Median sequence conservation: 3.50
    Sequences represented at this position:6

Substitution at pos 109 from P to T is predicted to be TOLERATED with a score of 0.24.
    Median sequence conservation: 3.50
    Sequences represented at this position:6

Substitution at pos 114 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 116 from R to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 133 from P to T is predicted to be TOLERATED with a score of 0.20.
    Median sequence conservation: 3.58
    Sequences represented at this position:6

Substitution at pos 150 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.48
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 157 from R to Q is predicted to be TOLERATED with a score of 0.33.
    Median sequence conservation: 3.48
    Sequences represented at this position:8

Substitution at pos 161 from Q to K is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.48
    Sequences represented at this position:8

Substitution at pos 165 from V to I is predicted to be TOLERATED with a score of 0.25.
    Median sequence conservation: 3.48
    Sequences represented at this position:8

Substitution at pos 191 from Q to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.37
    Sequences represented at this position:12
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 196 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.37
    Sequences represented at this position:12
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 207 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.38
    Sequences represented at this position:11
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 225 from T to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.32
    Sequences represented at this position:15
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 270 from R to Q is predicted to be TOLERATED with a score of 0.38.
    Median sequence conservation: 2.98
    Sequences represented at this position:77

Substitution at pos 276 from F to Y is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.21
    Sequences represented at this position:75

Substitution at pos 299 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:77

Substitution at pos 322 from R to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:78

Substitution at pos 332 from E to K is predicted to be TOLERATED with a score of 0.68.
    Median sequence conservation: 3.00
    Sequences represented at this position:73

Substitution at pos 335 from E to K is predicted to be TOLERATED with a score of 0.16.
    Median sequence conservation: 2.98
    Sequences represented at this position:78

Substitution at pos 353 from V to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:79

Substitution at pos 377 from Q to H is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 2.98
    Sequences represented at this position:79

Substitution at pos 391 from E to K is predicted to be TOLERATED with a score of 0.29.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 395 from Y to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 400 from S to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 468 from E to K is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 483 from M to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 485 from N to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 497 from V to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 515 from C to S is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 524 from R to H is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 526 from R to H is predicted to be TOLERATED with a score of 0.30.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 527 from F to L is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 540 from C to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 542 from A to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 578 from R to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 581 from L to Q is predicted to be TOLERATED with a score of 0.53.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 594 from P to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 621 from M to L is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 623 from E to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 631 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

Substitution at pos 672 from A to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:80

