
Predictions

Substitution at pos 29 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 45 from P to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 57 from D to N is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.41
    Sequences represented at this position:6

Substitution at pos 91 from G to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.40
    Sequences represented at this position:7
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 135 from R to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.40
    Sequences represented at this position:7
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 201 from H to Y is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.40
    Sequences represented at this position:7

Substitution at pos 211 from T to A is predicted to be TOLERATED with a score of 0.22.
    Median sequence conservation: 3.40
    Sequences represented at this position:7

Substitution at pos 218 from Q to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.40
    Sequences represented at this position:7
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 252 from P to S is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.40
    Sequences represented at this position:9

Substitution at pos 276 from D to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.40
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 281 from M to I is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.40
    Sequences represented at this position:9

Substitution at pos 282 from L to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.40
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 296 from C to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.40
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 344 from G to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.35
    Sequences represented at this position:13
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 356 from S to T is predicted to be TOLERATED with a score of 0.26.
    Median sequence conservation: 3.35
    Sequences represented at this position:13

Substitution at pos 401 from R to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.20
    Sequences represented at this position:23

Substitution at pos 405 from E to K is predicted to be TOLERATED with a score of 0.46.
    Median sequence conservation: 3.18
    Sequences represented at this position:24

Substitution at pos 436 from D to N is predicted to be TOLERATED with a score of 0.22.
    Median sequence conservation: 3.17
    Sequences represented at this position:24

Substitution at pos 449 from M to I is predicted to be TOLERATED with a score of 0.21.
    Median sequence conservation: 3.18
    Sequences represented at this position:33

Substitution at pos 470 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.18
    Sequences represented at this position:33

Substitution at pos 474 from V to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.18
    Sequences represented at this position:33

Substitution at pos 478 from A to V is predicted to be TOLERATED with a score of 0.16.
    Median sequence conservation: 3.18
    Sequences represented at this position:33

Substitution at pos 479 from L to V is predicted to be TOLERATED with a score of 0.16.
    Median sequence conservation: 3.18
    Sequences represented at this position:33

Substitution at pos 483 from A to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.18
    Sequences represented at this position:33

Substitution at pos 494 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.18
    Sequences represented at this position:33

Substitution at pos 499 from T to A is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.18
    Sequences represented at this position:31

Substitution at pos 532 from P to S is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.37
    Sequences represented at this position:24

Substitution at pos 543 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.18
    Sequences represented at this position:32

Substitution at pos 545 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.18
    Sequences represented at this position:32

Substitution at pos 562 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.18
    Sequences represented at this position:32

Substitution at pos 571 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.18
    Sequences represented at this position:32

Substitution at pos 573 from F to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.18
    Sequences represented at this position:32

Substitution at pos 576 from Q to H is predicted to be TOLERATED with a score of 0.38.
    Median sequence conservation: 3.18
    Sequences represented at this position:32

Substitution at pos 584 from N to S is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.18
    Sequences represented at this position:32

Substitution at pos 588 from V to I is predicted to be TOLERATED with a score of 0.40.
    Median sequence conservation: 3.18
    Sequences represented at this position:32

Substitution at pos 606 from R to G is predicted to be TOLERATED with a score of 0.45.
    Median sequence conservation: 3.22
    Sequences represented at this position:28

Substitution at pos 617 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.47
    Sequences represented at this position:12
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 624 from Q to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.49
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 637 from T to A is predicted to be TOLERATED with a score of 0.56.
    Median sequence conservation: 3.47
    Sequences represented at this position:9

Substitution at pos 641 from P to L is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.48
    Sequences represented at this position:9

Substitution at pos 648 from H to D is predicted to be TOLERATED with a score of 0.65.
    Median sequence conservation: 3.52
    Sequences represented at this position:8

Substitution at pos 650 from F to V is predicted to be TOLERATED with a score of 0.29.
    Median sequence conservation: 3.84
    Sequences represented at this position:9

Substitution at pos 664 from L to V is predicted to be TOLERATED with a score of 0.45.
    Median sequence conservation: 3.91
    Sequences represented at this position:6

Substitution at pos 666 from T to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.91
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 667 from E to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.91
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 671 from H to Y is predicted to be TOLERATED with a score of 0.19.
    Median sequence conservation: 3.91
    Sequences represented at this position:6

Substitution at pos 675 from Y to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.91
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 681 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.91
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 684 from E to K is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.91
    Sequences represented at this position:6

Substitution at pos 687 from V to L is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.93
    Sequences represented at this position:5

Substitution at pos 691 from T to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.92
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 714 from Q to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.40
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 716 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.40
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 728 from T to M is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.43
    Sequences represented at this position:8

Substitution at pos 744 from L to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.30
    Sequences represented at this position:12
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 753 from V to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.21
    Sequences represented at this position:13

Substitution at pos 772 from R to K is predicted to be TOLERATED with a score of 0.31.
    Median sequence conservation: 3.21
    Sequences represented at this position:13

Substitution at pos 774 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.30
    Sequences represented at this position:12
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 817 from H to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.32
    Sequences represented at this position:11
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 841 from P to T is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.33
    Sequences represented at this position:10

Substitution at pos 869 from M to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.40
    Sequences represented at this position:7
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 878 from D to H is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.34
    Sequences represented at this position:9

Substitution at pos 908 from V to L is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.33
    Sequences represented at this position:10

Substitution at pos 924 from T to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.35
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 944 from H to R is predicted to be TOLERATED with a score of 0.68.
    Median sequence conservation: 3.33
    Sequences represented at this position:10

Substitution at pos 945 from R to Q is predicted to be TOLERATED with a score of 0.28.
    Median sequence conservation: 3.33
    Sequences represented at this position:10

Substitution at pos 960 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.40
    Sequences represented at this position:7
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1015 from F to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.21
    Sequences represented at this position:16

Substitution at pos 1025 from R to C is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.21
    Sequences represented at this position:17

Substitution at pos 1034 from V to L is predicted to be TOLERATED with a score of 0.23.
    Median sequence conservation: 3.21
    Sequences represented at this position:17

Substitution at pos 1035 from V to L is predicted to be TOLERATED with a score of 0.25.
    Median sequence conservation: 3.21
    Sequences represented at this position:17

Substitution at pos 1075 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.21
    Sequences represented at this position:17

Substitution at pos 1076 from I to V is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.21
    Sequences represented at this position:17

Substitution at pos 1098 from V to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.21
    Sequences represented at this position:17

Substitution at pos 1100 from V to I is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.21
    Sequences represented at this position:17

Substitution at pos 1109 from G to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.21
    Sequences represented at this position:17

Substitution at pos 1119 from L to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.22
    Sequences represented at this position:16

Substitution at pos 1139 from M to I is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.21
    Sequences represented at this position:17

Substitution at pos 1162 from L to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.21
    Sequences represented at this position:16

Substitution at pos 1164 from V to L is predicted to be TOLERATED with a score of 0.59.
    Median sequence conservation: 3.21
    Sequences represented at this position:16

Substitution at pos 1167 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.21
    Sequences represented at this position:16

Substitution at pos 1169 from V to I is predicted to be TOLERATED with a score of 0.51.
    Median sequence conservation: 3.21
    Sequences represented at this position:16

Substitution at pos 1172 from P to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.21
    Sequences represented at this position:16

Substitution at pos 1188 from N to Y is predicted to be TOLERATED with a score of 0.24.
    Median sequence conservation: 3.40
    Sequences represented at this position:6

Substitution at pos 1195 from T to S is predicted to be TOLERATED with a score of 0.24.
    Median sequence conservation: 3.55
    Sequences represented at this position:4

Substitution at pos 1204 from V to A is predicted to be TOLERATED with a score of 0.36.
    Median sequence conservation: 4.01
    Sequences represented at this position:4

Substitution at pos 1205 from V to I is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.53
    Sequences represented at this position:5

Substitution at pos 1220 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.53
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1224 from E to D is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.53
    Sequences represented at this position:5

Substitution at pos 1239 from R to L is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 1247 from A to T is predicted to be TOLERATED with a score of 0.28.
    Median sequence conservation: 3.75
    Sequences represented at this position:3

Substitution at pos 1292 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1296 from G to E is predicted to be TOLERATED with a score of 0.29.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 1297 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1306 from P to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1314 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1315 from P to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1324 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1338 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1349 from P to R is predicted to be TOLERATED with a score of 0.19.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 1350 from R to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1363 from G to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1364 from Y to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1366 from Q to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1385 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1386 from V to A is predicted to be TOLERATED with a score of 0.76.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 1397 from L to I is predicted to be TOLERATED with a score of 0.19.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 1404 from T to A is predicted to be TOLERATED with a score of 0.39.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 1412 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1419 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1442 from R to L is predicted to be TOLERATED with a score of 0.22.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 1444 from S to G is predicted to be TOLERATED with a score of 0.29.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

