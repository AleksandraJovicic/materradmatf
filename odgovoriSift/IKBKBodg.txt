
Predictions

Substitution at pos 20 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.13
    Sequences represented at this position:341

Substitution at pos 34 from N to S is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.15
    Sequences represented at this position:392

Substitution at pos 82 from G to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.25
    Sequences represented at this position:280

Substitution at pos 131 from S to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.15
    Sequences represented at this position:395

Substitution at pos 143 from H to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.15
    Sequences represented at this position:396

Substitution at pos 144 from R to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.15
    Sequences represented at this position:396

Substitution at pos 157 from E to K is predicted to be TOLERATED with a score of 0.23.
    Median sequence conservation: 3.15
    Sequences represented at this position:368

Substitution at pos 171 from K to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.15
    Sequences represented at this position:396

Substitution at pos 172 from E to G is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.15
    Sequences represented at this position:389

Substitution at pos 204 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.15
    Sequences represented at this position:396

Substitution at pos 232 from H to R is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.14
    Sequences represented at this position:74

Substitution at pos 236 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.12
    Sequences represented at this position:60

Substitution at pos 261 from Y to C is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.18
    Sequences represented at this position:40

Substitution at pos 301 from K to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.33
    Sequences represented at this position:15
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 303 from L to M is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.33
    Sequences represented at this position:15

Substitution at pos 323 from H to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.38
    Sequences represented at this position:14
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 336 from L to F is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.33
    Sequences represented at this position:15

Substitution at pos 351 from Q to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.38
    Sequences represented at this position:14
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 357 from A to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.38
    Sequences represented at this position:14
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 361 from L to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.38
    Sequences represented at this position:12
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 374 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.61
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 414 from L to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.61
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 422 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.61
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 434 from W to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.61
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 446 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.61
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 470 from N to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.61
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 484 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.61
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 499 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.61
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 513 from A to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.61
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 526 from R to Q is predicted to be TOLERATED with a score of 0.98.
    Median sequence conservation: 3.61
    Sequences represented at this position:8

Substitution at pos 537 from M to I is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.61
    Sequences represented at this position:8

Substitution at pos 538 from M to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.61
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 582 from R to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.56
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 592 from R to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.61
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 639 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.61
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 710 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.61
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

