
Predictions

Substitution at pos 3 from A to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 18 from P to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 39 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:17
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 43 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 4.32
    Sequences represented at this position:22
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 56 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.08
    Sequences represented at this position:211
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 60 from V to A is predicted to be TOLERATED with a score of 0.36.
    Median sequence conservation: 4.08
    Sequences represented at this position:221

Substitution at pos 68 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.08
    Sequences represented at this position:221
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 69 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 4.08
    Sequences represented at this position:221
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 121 from G to S is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 4.10
    Sequences represented at this position:197

Substitution at pos 136 from D to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:167
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 157 from P to A is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 4.32
    Sequences represented at this position:41

Substitution at pos 169 from A to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 4.32
    Sequences represented at this position:28
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 174 from G to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 4.32
    Sequences represented at this position:17
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 212 from P to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 214 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 218 from C to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:2
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 285 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 301 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 319 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 349 from R to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 368 from G to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

