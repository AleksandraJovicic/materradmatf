
Predictions

Substitution at pos 3 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 4.32
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 36 from T to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 4.32
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 38 from D to V is predicted to be TOLERATED with a score of 0.50.
    Median sequence conservation: 4.32
    Sequences represented at this position:9

Substitution at pos 50 from S to F is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.93
    Sequences represented at this position:13

Substitution at pos 52 from S to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.80
    Sequences represented at this position:18
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 53 from A to T is predicted to be TOLERATED with a score of 0.62.
    Median sequence conservation: 3.69
    Sequences represented at this position:21

Substitution at pos 60 from H to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.69
    Sequences represented at this position:21
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 70 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.29
    Sequences represented at this position:29
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 71 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.29
    Sequences represented at this position:29
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 120 from R to M is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 123 from F to I is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 125 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 147 from P to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 169 from Y to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 170 from Q to L is predicted to be TOLERATED with a score of 0.18.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 176 from Q to H is predicted to be TOLERATED with a score of 0.24.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 183 from G to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.46
    Sequences represented at this position:26
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 188 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 212 from D to N is predicted to be TOLERATED with a score of 0.89.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 260 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 261 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 267 from T to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 273 from T to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 286 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 296 from H to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 310 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 331 from A to T is predicted to be TOLERATED with a score of 0.16.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 338 from A to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 339 from S to C is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 345 from R to Q is predicted to be TOLERATED with a score of 0.23.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 352 from R to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 383 from I to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 402 from V to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 404 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 416 from N to K is predicted to be TOLERATED with a score of 0.23.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 437 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 442 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 451 from R to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 455 from N to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 465 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 469 from L to M is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 472 from E to A is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 522 from H to Y is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 523 from A to V is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 583 from E to K is predicted to be TOLERATED with a score of 0.91.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 589 from R to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 593 from A to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.02
    Sequences represented at this position:34

Substitution at pos 606 from Y to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.62
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 637 from V to A is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 3.32
    Sequences represented at this position:24

Substitution at pos 657 from V to I is predicted to be TOLERATED with a score of 0.68.
    Median sequence conservation: 3.45
    Sequences represented at this position:22

Substitution at pos 662 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.45
    Sequences represented at this position:22
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

