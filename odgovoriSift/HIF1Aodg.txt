
Predictions

Substitution at pos 38 from S to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.13
    Sequences represented at this position:39

Substitution at pos 41 from R to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.12
    Sequences represented at this position:53

Substitution at pos 48 from D to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.10
    Sequences represented at this position:89

Substitution at pos 53 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.09
    Sequences represented at this position:94

Substitution at pos 100 from G to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.09
    Sequences represented at this position:94

Substitution at pos 145 from G to R is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 3.07
    Sequences represented at this position:105

Substitution at pos 195 from M to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.10
    Sequences represented at this position:87

Substitution at pos 220 from G to D is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.07
    Sequences represented at this position:98

Substitution at pos 243 from C to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.07
    Sequences represented at this position:99

Substitution at pos 403 from E to K is predicted to be TOLERATED with a score of 0.21.
    Median sequence conservation: 3.34
    Sequences represented at this position:19

Substitution at pos 438 from G to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.65
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 442 from T to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 457 from D to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.65
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 465 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.88
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 480 from A to T is predicted to be TOLERATED with a score of 0.28.
    Median sequence conservation: 3.65
    Sequences represented at this position:9

Substitution at pos 527 from D to N is predicted to be TOLERATED with a score of 0.36.
    Median sequence conservation: 3.59
    Sequences represented at this position:10

Substitution at pos 646 from D to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 4.32
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 654 from D to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 666 from P to S is predicted to be TOLERATED with a score of 0.38.
    Median sequence conservation: 4.32
    Sequences represented at this position:4

Substitution at pos 705 from E to D is predicted to be TOLERATED with a score of 0.50.
    Median sequence conservation: 4.32
    Sequences represented at this position:5

Substitution at pos 706 from K to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 709 from P to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 750 from G to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 767 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 829 from P to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.70
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

