
Predictions

Substitution at pos 6 from L to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 20 from R to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 23 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 85 from I to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 86 from S to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 113 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 136 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 137 from H to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 138 from T to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 179 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 203 from S to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.63
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 251 from L to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.25
    Sequences represented at this position:15

Substitution at pos 265 from V to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.19
    Sequences represented at this position:17

Substitution at pos 281 from I to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.15
    Sequences represented at this position:20

Substitution at pos 298 from P to T is predicted to be TOLERATED with a score of 0.25.
    Median sequence conservation: 3.10
    Sequences represented at this position:22

Substitution at pos 314 from L to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.10
    Sequences represented at this position:22

Substitution at pos 319 from L to F is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.10
    Sequences represented at this position:22

Substitution at pos 329 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.20
    Sequences represented at this position:22

Substitution at pos 354 from A to S is predicted to be TOLERATED with a score of 0.36.
    Median sequence conservation: 3.07
    Sequences represented at this position:25

Substitution at pos 362 from D to N is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 3.11
    Sequences represented at this position:26

Substitution at pos 367 from R to P is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.09
    Sequences represented at this position:33

Substitution at pos 375 from V to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.01
    Sequences represented at this position:74

Substitution at pos 378 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.03
    Sequences represented at this position:105

Substitution at pos 382 from H to Y is predicted to be TOLERATED with a score of 0.45.
    Median sequence conservation: 3.02
    Sequences represented at this position:91

Substitution at pos 383 from V to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.02
    Sequences represented at this position:101

Substitution at pos 388 from Q to H is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.03
    Sequences represented at this position:114

Substitution at pos 397 from G to D is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.01
    Sequences represented at this position:140

Substitution at pos 398 from S to Y is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.01
    Sequences represented at this position:140

Substitution at pos 423 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 2.99
    Sequences represented at this position:141

Substitution at pos 425 from W to L is predicted to be TOLERATED with a score of 0.27.
    Median sequence conservation: 2.99
    Sequences represented at this position:141

Substitution at pos 426 from S to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 2.98
    Sequences represented at this position:143

Substitution at pos 436 from S to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:148

Substitution at pos 440 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 2.98
    Sequences represented at this position:148

Substitution at pos 441 from R to L is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 2.98
    Sequences represented at this position:148

Substitution at pos 448 from A to G is predicted to be TOLERATED with a score of 0.33.
    Median sequence conservation: 2.98
    Sequences represented at this position:148

Substitution at pos 451 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 459 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 461 from T to A is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 465 from R to V is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 466 from C to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 471 from E to K is predicted to be TOLERATED with a score of 0.41.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 484 from R to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 490 from T to A is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 2.98
    Sequences represented at this position:148

Substitution at pos 500 from H to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 502 from A to V is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 503 from A to T is predicted to be TOLERATED with a score of 0.15.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 505 from R to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 2.99
    Sequences represented at this position:148

Substitution at pos 507 from V to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.00
    Sequences represented at this position:139

Substitution at pos 516 from S to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 518 from A to T is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 533 from C to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.00
    Sequences represented at this position:142

Substitution at pos 536 from T to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 2.99
    Sequences represented at this position:148

Substitution at pos 542 from N to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 543 from R to K is predicted to be TOLERATED with a score of 0.81.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 544 from V to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 545 from Y to S is predicted to be TOLERATED with a score of 0.31.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 546 from S to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 547 from L to I is predicted to be TOLERATED with a score of 0.19.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 557 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 560 from D to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 563 from I to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:148

Substitution at pos 566 from W to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.98
    Sequences represented at this position:149

Substitution at pos 573 from C to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 2.98
    Sequences represented at this position:147

Substitution at pos 575 from H to P is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 2.98
    Sequences represented at this position:148

Substitution at pos 588 from E to K is predicted to be TOLERATED with a score of 0.51.
    Median sequence conservation: 2.98
    Sequences represented at this position:141

Substitution at pos 597 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 2.99
    Sequences represented at this position:148

Substitution at pos 600 from D to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 2.99
    Sequences represented at this position:148

Substitution at pos 606 from W to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 2.99
    Sequences represented at this position:148

Substitution at pos 623 from H to Y is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.02
    Sequences represented at this position:134

Substitution at pos 625 from S to G is predicted to be TOLERATED with a score of 0.30.
    Median sequence conservation: 3.00
    Sequences represented at this position:137

Substitution at pos 626 from A to P is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.00
    Sequences represented at this position:140

Substitution at pos 641 from S to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.01
    Sequences represented at this position:135

Substitution at pos 667 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.33
    Sequences represented at this position:17
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 670 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.37
    Sequences represented at this position:16
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 674 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.37
    Sequences represented at this position:18
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 685 from A to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.50
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 689 from R to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.60
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

