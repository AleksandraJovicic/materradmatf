
Predictions

Substitution at pos 6 from P to Q is predicted to be TOLERATED with a score of 0.50.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 13 from C to S is predicted to be TOLERATED with a score of 0.14.
    Median sequence conservation: 3.55
    Sequences represented at this position:4

Substitution at pos 28 from P to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.44
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 34 from E to K is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:11

Substitution at pos 46 from S to Y is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 50 from F to L is predicted to be TOLERATED with a score of 0.36.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 59 from Y to N is predicted to be TOLERATED with a score of 0.31.
    Median sequence conservation: 3.15
    Sequences represented at this position:10

Substitution at pos 66 from S to N is predicted to be TOLERATED with a score of 0.49.
    Median sequence conservation: 3.11
    Sequences represented at this position:10

Substitution at pos 67 from S to T is predicted to be TOLERATED with a score of 0.46.
    Median sequence conservation: 3.11
    Sequences represented at this position:10

Substitution at pos 88 from S to N is predicted to be TOLERATED with a score of 0.76.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 100 from C to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 123 from D to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 129 from V to A is predicted to be TOLERATED with a score of 0.88.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 134 from T to M is predicted to be TOLERATED with a score of 0.14.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 135 from D to N is predicted to be TOLERATED with a score of 0.49.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 141 from E to K is predicted to be TOLERATED with a score of 0.90.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 147 from I to F is predicted to be TOLERATED with a score of 0.15.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 151 from R to H is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 160 from T to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 161 from L to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 175 from R to K is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 183 from T to I is predicted to be TOLERATED with a score of 0.19.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 185 from G to R is predicted to be TOLERATED with a score of 0.81.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 224 from V to M is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 3.15
    Sequences represented at this position:11

Substitution at pos 235 from C to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 251 from G to V is predicted to be TOLERATED with a score of 0.18.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 252 from E to D is predicted to be TOLERATED with a score of 0.21.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 257 from G to D is predicted to be TOLERATED with a score of 0.16.
    Median sequence conservation: 3.05
    Sequences represented at this position:11

Substitution at pos 266 from V to A is predicted to be TOLERATED with a score of 0.50.
    Median sequence conservation: 3.13
    Sequences represented at this position:11

Substitution at pos 267 from P to S is predicted to be TOLERATED with a score of 0.54.
    Median sequence conservation: 3.16
    Sequences represented at this position:10

Substitution at pos 271 from L to S is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 275 from L to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 279 from E to K is predicted to be TOLERATED with a score of 0.85.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 281 from T to P is predicted to be TOLERATED with a score of 0.27.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 288 from Y to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 301 from E to K is predicted to be TOLERATED with a score of 0.84.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 304 from K to R is predicted to be TOLERATED with a score of 0.46.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 340 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 341 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 345 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 355 from T to I is predicted to be TOLERATED with a score of 0.21.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 356 from L to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 372 from E to K is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 386 from E to K is predicted to be TOLERATED with a score of 0.25.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 389 from S to N is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 409 from L to V is predicted to be TOLERATED with a score of 0.21.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 415 from S to L is predicted to be TOLERATED with a score of 0.15.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 421 from V to F is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.39
    Sequences represented at this position:8

Substitution at pos 423 from D to A is predicted to be TOLERATED with a score of 0.72.
    Median sequence conservation: 3.39
    Sequences represented at this position:9

Substitution at pos 451 from K to T is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 3.15
    Sequences represented at this position:11

Substitution at pos 452 from D to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 459 from E to K is predicted to be TOLERATED with a score of 0.55.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 474 from T to M is predicted to be TOLERATED with a score of 0.89.
    Median sequence conservation: 3.32
    Sequences represented at this position:8

Substitution at pos 476 from I to V is predicted to be TOLERATED with a score of 0.43.
    Median sequence conservation: 3.42
    Sequences represented at this position:7

Substitution at pos 480 from D to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 491 from A to T is predicted to be TOLERATED with a score of 0.64.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 497 from I to V is predicted to be TOLERATED with a score of 0.41.
    Median sequence conservation: 3.33
    Sequences represented at this position:10

Substitution at pos 501 from C to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 506 from L to I is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 3.15
    Sequences represented at this position:11

Substitution at pos 519 from P to L is predicted to be TOLERATED with a score of 0.67.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 522 from R to H is predicted to be TOLERATED with a score of 0.55.
    Median sequence conservation: 3.15
    Sequences represented at this position:11

Substitution at pos 539 from I to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.13
    Sequences represented at this position:11

Substitution at pos 546 from V to I is predicted to be TOLERATED with a score of 0.97.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 558 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 563 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 564 from S to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 569 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 570 from H to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 577 from P to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 589 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 595 from L to F is predicted to be TOLERATED with a score of 0.28.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 628 from M to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 639 from Q to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 641 from L to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 649 from T to S is predicted to be TOLERATED with a score of 0.92.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 652 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 659 from N to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 675 from E to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 688 from K to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 689 from N to D is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 696 from H to Y is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.15
    Sequences represented at this position:11

Substitution at pos 702 from K to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.15
    Sequences represented at this position:11

Substitution at pos 709 from G to E is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 715 from E to D is predicted to be TOLERATED with a score of 0.57.
    Median sequence conservation: 3.13
    Sequences represented at this position:11

Substitution at pos 743 from V to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.13
    Sequences represented at this position:11

Substitution at pos 755 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 762 from Y to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.15
    Sequences represented at this position:11

Substitution at pos 775 from D to H is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.05
    Sequences represented at this position:11

Substitution at pos 785 from D to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.25
    Sequences represented at this position:11
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 791 from T to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 794 from D to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 819 from L to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 822 from R to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 824 from V to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 829 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 864 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 869 from D to N is predicted to be TOLERATED with a score of 0.71.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 882 from G to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 885 from L to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 891 from L to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 895 from P to L is predicted to be TOLERATED with a score of 0.25.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 898 from G to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 912 from G to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 914 from R to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 916 from A to T is predicted to be TOLERATED with a score of 0.42.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 922 from T to A is predicted to be TOLERATED with a score of 0.26.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 934 from N to H is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 937 from P to L is predicted to be TOLERATED with a score of 0.76.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 938 from E to Q is predicted to be TOLERATED with a score of 0.16.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 939 from K to N is predicted to be TOLERATED with a score of 0.30.
    Median sequence conservation: 3.06
    Sequences represented at this position:12

Substitution at pos 955 from P to H is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.33
    Sequences represented at this position:10

Substitution at pos 990 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.33
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 996 from E to K is predicted to be TOLERATED with a score of 0.29.
    Median sequence conservation: 3.40
    Sequences represented at this position:7

Substitution at pos 1014 from A to T is predicted to be TOLERATED with a score of 0.50.
    Median sequence conservation: 3.33
    Sequences represented at this position:10

Substitution at pos 1038 from N to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.42
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1054 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.42
    Sequences represented at this position:8
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1064 from D to E is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.47
    Sequences represented at this position:7

Substitution at pos 1065 from E to D is predicted to be TOLERATED with a score of 0.21.
    Median sequence conservation: 3.53
    Sequences represented at this position:6

Substitution at pos 1068 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.53
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1082 from D to G is predicted to be TOLERATED with a score of 0.15.
    Median sequence conservation: 3.53
    Sequences represented at this position:6

Substitution at pos 1089 from L to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.53
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

