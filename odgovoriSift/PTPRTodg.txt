
Predictions

Substitution at pos 6 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 50 from N to D is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.53
    Sequences represented at this position:9

Substitution at pos 83 from A to T is predicted to be TOLERATED with a score of 0.29.
    Median sequence conservation: 3.53
    Sequences represented at this position:9

Substitution at pos 84 from S to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.53
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 89 from H to N is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.53
    Sequences represented at this position:9

Substitution at pos 90 from L to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.53
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 99 from D to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.53
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 112 from R to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.53
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 115 from S to F is predicted to be TOLERATED with a score of 0.72.
    Median sequence conservation: 3.53
    Sequences represented at this position:9

Substitution at pos 128 from G to D is predicted to be TOLERATED with a score of 0.28.
    Median sequence conservation: 3.53
    Sequences represented at this position:9

Substitution at pos 129 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.53
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 134 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.53
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 144 from E to K is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.53
    Sequences represented at this position:9

Substitution at pos 149 from A to V is predicted to be TOLERATED with a score of 0.21.
    Median sequence conservation: 3.53
    Sequences represented at this position:9

Substitution at pos 152 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.53
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 155 from T to I is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.53
    Sequences represented at this position:9

Substitution at pos 157 from W to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.53
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 162 from Q to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.53
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 168 from V to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.53
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 174 from P to S is predicted to be TOLERATED with a score of 0.54.
    Median sequence conservation: 3.53
    Sequences represented at this position:9

Substitution at pos 178 from A to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.53
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 179 from V to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.53
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 207 from Q to H is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 3.36
    Sequences represented at this position:12

Substitution at pos 218 from K to E is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 3.36
    Sequences represented at this position:12

Substitution at pos 221 from Q to P is predicted to be TOLERATED with a score of 0.22.
    Median sequence conservation: 3.36
    Sequences represented at this position:12

Substitution at pos 227 from L to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.36
    Sequences represented at this position:12
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 233 from R to K is predicted to be TOLERATED with a score of 0.82.
    Median sequence conservation: 3.36
    Sequences represented at this position:12

Substitution at pos 235 from T to M is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 3.36
    Sequences represented at this position:12

Substitution at pos 246 from R to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.36
    Sequences represented at this position:12
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 259 from Q to K is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.53
    Sequences represented at this position:9

Substitution at pos 271 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.36
    Sequences represented at this position:12
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 280 from Y to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.36
    Sequences represented at this position:12
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 288 from P to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.36
    Sequences represented at this position:12
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 293 from A to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.36
    Sequences represented at this position:12
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 295 from P to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.36
    Sequences represented at this position:12
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 301 from G to A is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.36
    Sequences represented at this position:12

Substitution at pos 309 from P to K is predicted to be TOLERATED with a score of 0.19.
    Median sequence conservation: 3.33
    Sequences represented at this position:15

Substitution at pos 318 from G to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.53
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 328 from R to H is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.33
    Sequences represented at this position:15

Substitution at pos 341 from D to N is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.36
    Sequences represented at this position:12

Substitution at pos 344 from N to K is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.36
    Sequences represented at this position:12

Substitution at pos 351 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 352 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 357 from E to D is predicted to be TOLERATED with a score of 0.30.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 372 from P to L is predicted to be TOLERATED with a score of 0.26.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 385 from D to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.07
    Sequences represented at this position:14

Substitution at pos 386 from P to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.07
    Sequences represented at this position:14

Substitution at pos 392 from N to K is predicted to be TOLERATED with a score of 0.62.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 400 from A to T is predicted to be TOLERATED with a score of 0.30.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 402 from Q to K is predicted to be TOLERATED with a score of 0.86.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 416 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.18
    Sequences represented at this position:16

Substitution at pos 421 from N to I is predicted to be TOLERATED with a score of 0.26.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 431 from N to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 453 from R to C is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.04
    Sequences represented at this position:16

Substitution at pos 459 from R to W is predicted to be TOLERATED with a score of 0.18.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 464 from L to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 472 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 474 from E to K is predicted to be TOLERATED with a score of 0.43.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 483 from D to N is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 485 from P to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 489 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 496 from G to R is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.13
    Sequences represented at this position:16

Substitution at pos 531 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.20
    Sequences represented at this position:11

Substitution at pos 532 from S to G is predicted to be TOLERATED with a score of 0.37.
    Median sequence conservation: 3.07
    Sequences represented at this position:14

Substitution at pos 533 from A to S is predicted to be TOLERATED with a score of 0.43.
    Median sequence conservation: 3.07
    Sequences represented at this position:14

Substitution at pos 546 from R to Q is predicted to be TOLERATED with a score of 0.41.
    Median sequence conservation: 3.13
    Sequences represented at this position:14

Substitution at pos 555 from G to S is predicted to be TOLERATED with a score of 0.37.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 570 from T to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 575 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 595 from D to N is predicted to be TOLERATED with a score of 0.24.
    Median sequence conservation: 3.07
    Sequences represented at this position:14

Substitution at pos 605 from T to K is predicted to be TOLERATED with a score of 0.35.
    Median sequence conservation: 3.07
    Sequences represented at this position:14

Substitution at pos 606 from T to N is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 3.07
    Sequences represented at this position:14

Substitution at pos 617 from R to Q is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.07
    Sequences represented at this position:13

Substitution at pos 618 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.07
    Sequences represented at this position:13

Substitution at pos 625 from Q to E is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 3.31
    Sequences represented at this position:12

Substitution at pos 629 from K to N is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.36
    Sequences represented at this position:10

Substitution at pos 630 from E to Q is predicted to be TOLERATED with a score of 0.15.
    Median sequence conservation: 3.36
    Sequences represented at this position:10

Substitution at pos 632 from R to Q is predicted to be TOLERATED with a score of 0.30.
    Median sequence conservation: 3.36
    Sequences represented at this position:10

Substitution at pos 637 from R to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.36
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 639 from A to V is predicted to be TOLERATED with a score of 0.81.
    Median sequence conservation: 3.36
    Sequences represented at this position:10

Substitution at pos 648 from V to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.36
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 649 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.53
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 650 from V to M is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.36
    Sequences represented at this position:10

Substitution at pos 657 from S to G is predicted to be TOLERATED with a score of 0.40.
    Median sequence conservation: 3.36
    Sequences represented at this position:10

Substitution at pos 658 from L to I is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 3.36
    Sequences represented at this position:10

Substitution at pos 660 from S to Y is predicted to be TOLERATED with a score of 0.14.
    Median sequence conservation: 3.36
    Sequences represented at this position:10

Substitution at pos 664 from F to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.36
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 666 from A to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.36
    Sequences represented at this position:10
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 689 from G to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.20
    Sequences represented at this position:11

Substitution at pos 692 from N to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.20
    Sequences represented at this position:11

Substitution at pos 696 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.20
    Sequences represented at this position:11

Substitution at pos 718 from N to K is predicted to be TOLERATED with a score of 0.14.
    Median sequence conservation: 3.20
    Sequences represented at this position:11

Substitution at pos 726 from A to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.20
    Sequences represented at this position:10

Substitution at pos 727 from P to T is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.39
    Sequences represented at this position:4

Substitution at pos 736 from G to V is predicted to be TOLERATED with a score of 0.32.
    Median sequence conservation: 3.58
    Sequences represented at this position:3

Substitution at pos 745 from G to V is predicted to be TOLERATED with a score of 0.20.
    Median sequence conservation: 3.58
    Sequences represented at this position:3

Substitution at pos 748 from T to N is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.32
    Sequences represented at this position:8

Substitution at pos 762 from N to D is predicted to be TOLERATED with a score of 0.49.
    Median sequence conservation: 3.36
    Sequences represented at this position:10

Substitution at pos 763 from T to I is predicted to be TOLERATED with a score of 0.26.
    Median sequence conservation: 3.36
    Sequences represented at this position:10

Substitution at pos 769 from V to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.20
    Sequences represented at this position:11

Substitution at pos 771 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.20
    Sequences represented at this position:11

Substitution at pos 805 from K to N is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.36
    Sequences represented at this position:10

Substitution at pos 813 from A to T is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.36
    Sequences represented at this position:10

Substitution at pos 846 from V to I is predicted to be TOLERATED with a score of 0.52.
    Median sequence conservation: 3.20
    Sequences represented at this position:11

Substitution at pos 847 from N to D is predicted to be TOLERATED with a score of 0.39.
    Median sequence conservation: 3.36
    Sequences represented at this position:10

Substitution at pos 854 from R to L is predicted to be TOLERATED with a score of 0.30.
    Median sequence conservation: 3.20
    Sequences represented at this position:11

Substitution at pos 855 from G to R is predicted to be TOLERATED with a score of 0.42.
    Median sequence conservation: 3.20
    Sequences represented at this position:11

Substitution at pos 857 from L to F is predicted to be TOLERATED with a score of 0.75.
    Median sequence conservation: 3.20
    Sequences represented at this position:11

Substitution at pos 863 from T to R is predicted to be TOLERATED with a score of 0.42.
    Median sequence conservation: 3.20
    Sequences represented at this position:11

Substitution at pos 868 from P to S is predicted to be TOLERATED with a score of 0.18.
    Median sequence conservation: 3.07
    Sequences represented at this position:13

Substitution at pos 870 from R to H is predicted to be TOLERATED with a score of 0.29.
    Median sequence conservation: 3.19
    Sequences represented at this position:12

Substitution at pos 877 from M to I is predicted to be TOLERATED with a score of 0.56.
    Median sequence conservation: 3.05
    Sequences represented at this position:16

Substitution at pos 880 from P to H is predicted to be TOLERATED with a score of 0.27.
    Median sequence conservation: 3.29
    Sequences represented at this position:13

Substitution at pos 881 from R to L is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 3.15
    Sequences represented at this position:15

Substitution at pos 883 from Q to K is predicted to be TOLERATED with a score of 0.31.
    Median sequence conservation: 3.15
    Sequences represented at this position:15

Substitution at pos 889 from R to Q is predicted to be TOLERATED with a score of 0.31.
    Median sequence conservation: 3.13
    Sequences represented at this position:16

Substitution at pos 892 from D to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 901 from K to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 902 from R to K is predicted to be TOLERATED with a score of 0.24.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 904 from Q to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 909 from K to E is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 911 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 912 from Y to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 919 from Q to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.30
    Sequences represented at this position:13
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 922 from S to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.30
    Sequences represented at this position:13
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 924 from D to E is predicted to be TOLERATED with a score of 0.52.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 925 from T to I is predicted to be TOLERATED with a score of 0.50.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 930 from E to K is predicted to be TOLERATED with a score of 0.88.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 936 from R to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 947 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 970 from Y to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 980 from Q to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 985 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1007 from N to S is predicted to be TOLERATED with a score of 0.14.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1028 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1038 from E to D is predicted to be TOLERATED with a score of 0.24.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1057 from E to D is predicted to be TOLERATED with a score of 0.32.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1058 from I to M is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1068 from S to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1073 from G to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1086 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1089 from K to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1093 from P to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1094 from P to L is predicted to be TOLERATED with a score of 0.93.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1095 from E to D is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1110 from T to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1134 from C to S is predicted to be TOLERATED with a score of 0.67.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1142 from R to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1143 from V to I is predicted to be TOLERATED with a score of 0.87.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1163 from C to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1165 from C to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1175 from F to L is predicted to be TOLERATED with a score of 0.59.
    Median sequence conservation: 3.36
    Sequences represented at this position:10

Substitution at pos 1189 from T to K is predicted to be TOLERATED with a score of 0.19.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1190 from N to H is predicted to be TOLERATED with a score of 0.20.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1197 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1208 from V to M is predicted to be TOLERATED with a score of 0.20.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1210 from P to L is predicted to be TOLERATED with a score of 0.21.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1221 from N to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1224 from K to R is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1230 from V to A is predicted to be TOLERATED with a score of 0.24.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1238 from P to S is predicted to be TOLERATED with a score of 0.39.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1246 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.18
    Sequences represented at this position:16

Substitution at pos 1252 from N to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1253 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1266 from V to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1268 from T to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1272 from L to V is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1275 from T to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1279 from F to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1296 from E to A is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1297 from M to I is predicted to be TOLERATED with a score of 0.18.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1324 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.18
    Sequences represented at this position:16

Substitution at pos 1334 from R to K is predicted to be TOLERATED with a score of 0.61.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1343 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1346 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.18
    Sequences represented at this position:16

Substitution at pos 1370 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1377 from R to G is predicted to be TOLERATED with a score of 0.57.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1378 from R to S is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1383 from Q to K is predicted to be TOLERATED with a score of 0.75.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1389 from R to W is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 3.07
    Sequences represented at this position:16

Substitution at pos 1390 from E to K is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1396 from H to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1398 from L to P is predicted to be TOLERATED with a score of 0.19.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1401 from G to E is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1416 from M to I is predicted to be TOLERATED with a score of 0.21.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1418 from Q to R is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1424 from D to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1431 from T to A is predicted to be TOLERATED with a score of 0.20.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

Substitution at pos 1451 from E to K is predicted to be TOLERATED with a score of 0.45.
    Median sequence conservation: 3.04
    Sequences represented at this position:17

