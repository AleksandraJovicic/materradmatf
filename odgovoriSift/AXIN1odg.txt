
Predictions

Substitution at pos 9 from P to F is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 4.32
    Sequences represented at this position:6

Substitution at pos 19 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.32
    Sequences represented at this position:11
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 20 from A to S is predicted to be TOLERATED with a score of 0.23.
    Median sequence conservation: 3.32
    Sequences represented at this position:11

Substitution at pos 24 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 60 from T to S is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.36
    Sequences represented at this position:16

Substitution at pos 61 from P to A is predicted to be TOLERATED with a score of 0.64.
    Median sequence conservation: 3.36
    Sequences represented at this position:16

Substitution at pos 69 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.27
    Sequences represented at this position:18
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 73 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.04
    Sequences represented at this position:24

Substitution at pos 88 from S to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.03
    Sequences represented at this position:60

Substitution at pos 98 from G to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.03
    Sequences represented at this position:60

Substitution at pos 103 from R to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.03
    Sequences represented at this position:60

Substitution at pos 134 from E to K is predicted to be TOLERATED with a score of 0.14.
    Median sequence conservation: 3.15
    Sequences represented at this position:57

Substitution at pos 137 from R to K is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.04
    Sequences represented at this position:45

Substitution at pos 161 from K to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.03
    Sequences represented at this position:60

Substitution at pos 199 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.03
    Sequences represented at this position:60

Substitution at pos 202 from L to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.03
    Sequences represented at this position:60

Substitution at pos 207 from Y to C is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 3.03
    Sequences represented at this position:59

Substitution at pos 213 from T to I is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.04
    Sequences represented at this position:23

Substitution at pos 253 from M to I is predicted to be TOLERATED with a score of 0.44.
    Median sequence conservation: 3.25
    Sequences represented at this position:9

Substitution at pos 263 from P to S is predicted to be TOLERATED with a score of 0.20.
    Median sequence conservation: 3.08
    Sequences represented at this position:11

Substitution at pos 280 from V to I is predicted to be TOLERATED with a score of 0.44.
    Median sequence conservation: 3.16
    Sequences represented at this position:11

Substitution at pos 285 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 308 from A to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.32
    Sequences represented at this position:11
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 309 from G to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.32
    Sequences represented at this position:11
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 315 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 348 from I to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 359 from S to N is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 365 from R to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 383 from V to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 386 from Q to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 396 from L to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 403 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 414 from K to R is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.32
    Sequences represented at this position:11

Substitution at pos 416 from V to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.32
    Sequences represented at this position:11
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 424 from D to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.32
    Sequences represented at this position:11
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 426 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.32
    Sequences represented at this position:11
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 491 from H to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 492 from R to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 510 from A to S is predicted to be TOLERATED with a score of 0.65.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 511 from S to L is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 518 from P to S is predicted to be TOLERATED with a score of 0.61.
    Median sequence conservation: 3.32
    Sequences represented at this position:11

Substitution at pos 522 from A to T is predicted to be TOLERATED with a score of 0.45.
    Median sequence conservation: 3.38
    Sequences represented at this position:10

Substitution at pos 527 from A to T is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 3.38
    Sequences represented at this position:10

Substitution at pos 530 from H to Y is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.38
    Sequences represented at this position:10

Substitution at pos 543 from T to A is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.38
    Sequences represented at this position:10

Substitution at pos 572 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.42
    Sequences represented at this position:6
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 582 from V to A is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.38
    Sequences represented at this position:10

Substitution at pos 583 from G to S is predicted to be TOLERATED with a score of 0.59.
    Median sequence conservation: 3.38
    Sequences represented at this position:10

Substitution at pos 595 from S to G is predicted to be TOLERATED with a score of 0.45.
    Median sequence conservation: 3.32
    Sequences represented at this position:11

Substitution at pos 603 from K to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.32
    Sequences represented at this position:11
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 611 from S to P is predicted to be TOLERATED with a score of 0.32.
    Median sequence conservation: 3.32
    Sequences represented at this position:11

Substitution at pos 613 from K to N is predicted to be TOLERATED with a score of 0.28.
    Median sequence conservation: 3.32
    Sequences represented at this position:11

Substitution at pos 640 from E to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.32
    Sequences represented at this position:11
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 644 from S to N is predicted to be TOLERATED with a score of 0.21.
    Median sequence conservation: 3.43
    Sequences represented at this position:8

Substitution at pos 655 from G to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.32
    Sequences represented at this position:11
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 656 from T to M is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.32
    Sequences represented at this position:11

Substitution at pos 667 from P to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.46
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 676 from G to V is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 3.38
    Sequences represented at this position:10

Substitution at pos 678 from Q to K is predicted to be TOLERATED with a score of 0.26.
    Median sequence conservation: 3.38
    Sequences represented at this position:10

Substitution at pos 682 from S to F is predicted to be TOLERATED with a score of 0.28.
    Median sequence conservation: 3.32
    Sequences represented at this position:11

Substitution at pos 699 from P to S is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.32
    Sequences represented at this position:11

Substitution at pos 734 from V to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.16
    Sequences represented at this position:8

Substitution at pos 747 from A to T is predicted to be TOLERATED with a score of 0.45.
    Median sequence conservation: 3.38
    Sequences represented at this position:6

Substitution at pos 757 from S to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.16
    Sequences represented at this position:8

Substitution at pos 765 from E to G is predicted to be TOLERATED with a score of 0.26.
    Median sequence conservation: 3.16
    Sequences represented at this position:8

Substitution at pos 767 from R to T is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 779 from P to L is predicted to be TOLERATED with a score of 0.29.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 783 from I to V is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 795 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 804 from A to T is predicted to be TOLERATED with a score of 0.36.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 809 from Q to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 842 from E to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 845 from A to S is predicted to be TOLERATED with a score of 0.36.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

Substitution at pos 851 from E to D is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.07
    Sequences represented at this position:12

