
Predictions

Substitution at pos 31 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 33 from T to N is predicted to be TOLERATED with a score of 0.14.
    Median sequence conservation: 4.32
    Sequences represented at this position:2

Substitution at pos 37 from T to A is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 49 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 50 from L to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 52 from T to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 80 from N to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 82 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 94 from S to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:1
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 107 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 121 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 122 from T to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 133 from N to S is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 141 from G to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 150 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 159 from P to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 176 from S to G is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 177 from S to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 210 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 254 from V to I is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 261 from G to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 309 from H to L is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 314 from V to A is predicted to be TOLERATED with a score of 0.50.
    Median sequence conservation: 4.32
    Sequences represented at this position:3

Substitution at pos 319 from T to I is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 341 from R to K is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.62
    Sequences represented at this position:4

Substitution at pos 353 from E to K is predicted to be TOLERATED with a score of 0.30.
    Median sequence conservation: 3.62
    Sequences represented at this position:4

Substitution at pos 378 from F to L is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.62
    Sequences represented at this position:4

Substitution at pos 381 from A to T is predicted to be TOLERATED with a score of 0.23.
    Median sequence conservation: 3.62
    Sequences represented at this position:4

Substitution at pos 400 from C to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.48
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 407 from Q to E is predicted to be TOLERATED with a score of 0.09.
    Median sequence conservation: 3.48
    Sequences represented at this position:5

Substitution at pos 445 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.48
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 453 from T to P is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.48
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 471 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.48
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 472 from S to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.48
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 490 from N to K is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 3.48
    Sequences represented at this position:5

Substitution at pos 530 from R to I is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.48
    Sequences represented at this position:5

Substitution at pos 535 from K to E is predicted to be TOLERATED with a score of 0.05.
    Median sequence conservation: 3.48
    Sequences represented at this position:5

Substitution at pos 539 from F to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.48
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 540 from R to C is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.48
    Sequences represented at this position:5

Substitution at pos 544 from L to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.48
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 562 from P to H is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.48
    Sequences represented at this position:5

Substitution at pos 570 from H to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.48
    Sequences represented at this position:5
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 582 from F to Y is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.24
    Sequences represented at this position:13

Substitution at pos 583 from L to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.24
    Sequences represented at this position:13

Substitution at pos 584 from A to V is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.24
    Sequences represented at this position:13

Substitution at pos 592 from I to K is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.19
    Sequences represented at this position:15

Substitution at pos 594 from L to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.19
    Sequences represented at this position:15

Substitution at pos 596 from V to I is predicted to be TOLERATED with a score of 0.15.
    Median sequence conservation: 3.19
    Sequences represented at this position:15

Substitution at pos 610 from C to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.17
    Sequences represented at this position:17

Substitution at pos 638 from L to F is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.07
    Sequences represented at this position:49

Substitution at pos 640 from E to Q is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.06
    Sequences represented at this position:50

Substitution at pos 659 from S to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.05
    Sequences represented at this position:80

Substitution at pos 662 from R to Q is predicted to AFFECT PROTEIN FUNCTION with a score of 0.05.
    Median sequence conservation: 3.05
    Sequences represented at this position:76

Substitution at pos 686 from I to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.02
    Sequences represented at this position:113

Substitution at pos 687 from L to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.02
    Sequences represented at this position:113

Substitution at pos 745 from A to V is predicted to be TOLERATED with a score of 0.42.
    Median sequence conservation: 3.01
    Sequences represented at this position:114

Substitution at pos 747 from V to A is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.01
    Sequences represented at this position:114

Substitution at pos 775 from A to T is predicted to be TOLERATED with a score of 0.89.
    Median sequence conservation: 3.01
    Sequences represented at this position:113

Substitution at pos 785 from Q to K is predicted to be TOLERATED with a score of 0.31.
    Median sequence conservation: 3.02
    Sequences represented at this position:113

Substitution at pos 801 from N to S is predicted to be TOLERATED with a score of 0.13.
    Median sequence conservation: 3.03
    Sequences represented at this position:97

Substitution at pos 806 from A to T is predicted to be TOLERATED with a score of 0.14.
    Median sequence conservation: 3.03
    Sequences represented at this position:100

Substitution at pos 854 from S to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.01
    Sequences represented at this position:114

Substitution at pos 880 from D to N is predicted to be TOLERATED with a score of 0.20.
    Median sequence conservation: 3.01
    Sequences represented at this position:114

Substitution at pos 896 from V to I is predicted to be TOLERATED with a score of 0.16.
    Median sequence conservation: 3.01
    Sequences represented at this position:114

Substitution at pos 927 from P to L is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.26
    Sequences represented at this position:46

Substitution at pos 928 from Y to H is predicted to be TOLERATED with a score of 0.45.
    Median sequence conservation: 3.26
    Sequences represented at this position:46

Substitution at pos 979 from P to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.01
    Sequences represented at this position:114

Substitution at pos 990 from E to D is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 3.24
    Sequences represented at this position:45

Substitution at pos 1000 from D to N is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.16
    Sequences represented at this position:20

Substitution at pos 1002 from D to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.15
    Sequences represented at this position:24

Substitution at pos 1015 from S to N is predicted to be TOLERATED with a score of 0.14.
    Median sequence conservation: 3.21
    Sequences represented at this position:72

Substitution at pos 1018 from I to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.01
    Sequences represented at this position:114

Substitution at pos 1027 from W to C is predicted to be TOLERATED with a score of 0.16.
    Median sequence conservation: 3.03
    Sequences represented at this position:110

Substitution at pos 1045 from D to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.01
    Sequences represented at this position:114

Substitution at pos 1079 from G to E is predicted to be TOLERATED with a score of 0.36.
    Median sequence conservation: 3.01
    Sequences represented at this position:114

Substitution at pos 1103 from V to I is predicted to be TOLERATED with a score of 0.19.
    Median sequence conservation: 3.02
    Sequences represented at this position:108

Substitution at pos 1112 from K to N is predicted to be TOLERATED with a score of 0.19.
    Median sequence conservation: 3.02
    Sequences represented at this position:108

Substitution at pos 1115 from R to Q is predicted to be TOLERATED with a score of 0.06.
    Median sequence conservation: 3.02
    Sequences represented at this position:112

Substitution at pos 1119 from Q to L is predicted to be TOLERATED with a score of 0.08.
    Median sequence conservation: 3.02
    Sequences represented at this position:112

Substitution at pos 1128 from E to K is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 3.01
    Sequences represented at this position:114

Substitution at pos 1136 from E to K is predicted to be TOLERATED with a score of 0.29.
    Median sequence conservation: 3.01
    Sequences represented at this position:114

Substitution at pos 1162 from S to G is predicted to be TOLERATED with a score of 0.35.
    Median sequence conservation: 3.07
    Sequences represented at this position:80

Substitution at pos 1164 from P to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.03
    Sequences represented at this position:104

Substitution at pos 1172 from G to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.01
    Sequences represented at this position:114

Substitution at pos 1190 from E to K is predicted to be TOLERATED with a score of 0.22.
    Median sequence conservation: 3.01
    Sequences represented at this position:114

Substitution at pos 1203 from A to V is predicted to be TOLERATED with a score of 0.17.
    Median sequence conservation: 3.01
    Sequences represented at this position:114

Substitution at pos 1236 from V to E is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.35
    Sequences represented at this position:12
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1240 from N to S is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.38
    Sequences represented at this position:9
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1266 from G to D is predicted to be TOLERATED with a score of 0.38.
    Median sequence conservation: 3.62
    Sequences represented at this position:4

Substitution at pos 1271 from L to F is predicted to be TOLERATED with a score of 0.12.
    Median sequence conservation: 3.62
    Sequences represented at this position:4

Substitution at pos 1285 from S to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.62
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1297 from P to H is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.62
    Sequences represented at this position:4
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 1306 from S to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 4.32
    Sequences represented at this position:3
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

