
Predictions

Substitution at pos 22 from P to L is predicted to be TOLERATED with a score of 0.10.
    Median sequence conservation: 3.61
    Sequences represented at this position:12

Substitution at pos 46 from L to F is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.74
    Sequences represented at this position:12
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 70 from D to N is predicted to AFFECT PROTEIN FUNCTION with a score of 0.03.
    Median sequence conservation: 3.13
    Sequences represented at this position:18

Substitution at pos 76 from S to L is predicted to be TOLERATED with a score of 0.15.
    Median sequence conservation: 3.13
    Sequences represented at this position:18

Substitution at pos 80 from A to V is predicted to be TOLERATED with a score of 0.21.
    Median sequence conservation: 3.13
    Sequences represented at this position:18

Substitution at pos 99 from A to V is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.13
    Sequences represented at this position:18

Substitution at pos 128 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.13
    Sequences represented at this position:18

Substitution at pos 176 from A to T is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.13
    Sequences represented at this position:18

Substitution at pos 235 from A to S is predicted to be TOLERATED with a score of 1.00.
    Median sequence conservation: 3.13
    Sequences represented at this position:18

Substitution at pos 238 from R to W is predicted to AFFECT PROTEIN FUNCTION with a score of 0.00.
    Median sequence conservation: 3.13
    Sequences represented at this position:18

Substitution at pos 246 from V to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.13
    Sequences represented at this position:18

Substitution at pos 270 from P to L is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.60
    Sequences represented at this position:13
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 279 from F to C is predicted to AFFECT PROTEIN FUNCTION with a score of 0.02.
    Median sequence conservation: 3.41
    Sequences represented at this position:14
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 299 from R to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 3.63
    Sequences represented at this position:12
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 300 from P to R is predicted to AFFECT PROTEIN FUNCTION with a score of 0.04.
    Median sequence conservation: 3.63
    Sequences represented at this position:12
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

Substitution at pos 341 from D to H is predicted to be TOLERATED with a score of 0.11.
    Median sequence conservation: 3.13
    Sequences represented at this position:18

Substitution at pos 353 from N to I is predicted to be TOLERATED with a score of 0.32.
    Median sequence conservation: 3.13
    Sequences represented at this position:18

Substitution at pos 372 from S to F is predicted to be TOLERATED with a score of 0.07.
    Median sequence conservation: 3.13
    Sequences represented at this position:18

Substitution at pos 374 from V to L is predicted to be TOLERATED with a score of 0.52.
    Median sequence conservation: 3.13
    Sequences represented at this position:18

Substitution at pos 388 from R to G is predicted to be TOLERATED with a score of 0.79.
    Median sequence conservation: 3.51
    Sequences represented at this position:15

Substitution at pos 396 from T to M is predicted to AFFECT PROTEIN FUNCTION with a score of 0.01.
    Median sequence conservation: 4.32
    Sequences represented at this position:11
WARNING!!  This substitution may have been predicted to affect function just because 
 the sequences used were not diverse enough.  There is LOW CONFIDENCE in this prediction.

